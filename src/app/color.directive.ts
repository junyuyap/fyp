import { Directive, ElementRef, Renderer2, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective {

  constructor(public elRef: ElementRef, public renderer: Renderer2) { }

  @HostBinding('style.background-color') color: string

  @HostListener ('mouseenter') mouseOver(eventData: Event) {
    this.color = 'lightgray'
  }

  @HostListener ('mouseleave') mouseOut(eventData: Event) {
    this.color = 'white'
  }

}
