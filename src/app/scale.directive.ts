import { Directive, ElementRef, Renderer2, HostBinding, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appScale]'
})
export class ScaleDirective {

  constructor(public elRef: ElementRef, public renderer: Renderer2) { }

  @HostBinding('style.transform') transform: string

  @HostBinding('style.transition') transition: string

  ngOnInit() {
    this.transition = "all 0.3s"
  }

  @HostListener ('mouseenter') mouseOver(eventData: Event) {
    this.transform = 'scale(1.05)'
  }

  @HostListener ('mouseleave') mouseOut(eventData: Event) {
    this.transform = 'scale(1)'
  }

}
