import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './pages/header/header.component'
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { AssignmentComponent } from './pages/assignment/assignment.component';
import { StaffComponent } from './pages/staff/staff.component';
import { ModuleComponent } from './pages/module/module.component';
import { StaffinfoComponent } from './pages/staffinfo/staffinfo.component';
import { ExamComponent } from './pages/exam/exam.component';
import { MainComponent } from './pages/main/main.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { NavanimationDirective } from './navanimation.directive';
import { ShadowDirective } from './shadow.directive';
import { ScaleDirective } from './scale.directive';
import { FooterComponent } from './pages/footer/footer.component';
import { AdminComponent } from './pages/admin/admin.component';
import { LoginComponent } from './pages/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ColorDirective } from './color.directive';
import { StaffreqComponent } from './pages/staffinfo/staffreq/staffreq.component';
import { StaffpassComponent } from './pages/staffinfo/staffpass/staffpass.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SectionInfoComponent } from './pages/assignment/sub/section-info/section-info.component';
import { MatTableModule} from '@angular/material/table';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule} from '@angular/material/input';
import { TasComponent } from './pages/assignment/sub/tas/tas.component';
import { AddstaffComponent } from './pages/staff/addstaff/addstaff.component';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from "@angular/material/dialog";
import { ForecastAssignComponent } from './pages/assignment/sub/forecast-assign/forecast-assign.component';
import { CourseinfoComponent } from './pages/courseinfo/courseinfo.component';

@NgModule({
  declarations: [
    AppComponent,
    MaintenanceComponent,
    AssignmentComponent,
    StaffComponent,
    ModuleComponent,
    StaffinfoComponent,
    ExamComponent,
    HeaderComponent,
    MainComponent,
    NotfoundComponent,
    NavanimationDirective,
    ShadowDirective,
    ScaleDirective,
    FooterComponent,
    AdminComponent,
    LoginComponent,
    ColorDirective,
    StaffreqComponent,
    StaffpassComponent,
    SectionInfoComponent,
    TasComponent,
    AddstaffComponent,
    ForecastAssignComponent,
    CourseinfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    NgbModule,
    MatExpansionModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
