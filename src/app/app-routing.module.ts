import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from "./auth/auth-guard.service";
import { AssignmentComponent } from "./pages/assignment/assignment.component";
import { ExamComponent } from "./pages/exam/exam.component";
import { MaintenanceComponent } from "./pages/maintenance/maintenance.component"; 
import { ModuleComponent } from "./pages/module/module.component";
import { StaffComponent } from "./pages/staff/staff.component";
import { StaffinfoComponent } from "./pages/staffinfo/staffinfo.component";
import { MainComponent } from "./pages/main/main.component";
import { NotfoundComponent } from "./pages/notfound/notfound.component";
import { AdminComponent } from "./pages/admin/admin.component";
import { LoginComponent } from "./pages/login/login.component";
import { StaffreqComponent } from "./pages/staffinfo/staffreq/staffreq.component";
import { StaffpassComponent } from "./pages/staffinfo/staffpass/staffpass.component";
import { AddstaffComponent } from "./pages/staff/addstaff/addstaff.component";
import { ForecastAssignComponent } from './pages/assignment/sub/forecast-assign/forecast-assign.component';
import { CourseinfoComponent } from './pages/courseinfo/courseinfo.component'

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'main', canActivate: [AuthGuardService], component: MainComponent},
  {path: 'admin', canActivate: [AuthGuardService], component: AdminComponent},
  {path: 'module', canActivate: [AuthGuardService], component: ModuleComponent},
  {path: 'staff', canActivate: [AuthGuardService], component: StaffComponent},
  {path: 'addstaff', canActivate: [AuthGuardService], component: AddstaffComponent},
  {path: 'staffinfo', canActivate: [AuthGuardService], component: StaffinfoComponent},
  {path: 'maintenance', canActivate: [AuthGuardService], component: MaintenanceComponent},
  {path: 'staffreq', canActivate: [AuthGuardService], component: StaffreqComponent},
  {path: 'staffpass', canActivate: [AuthGuardService], component: StaffpassComponent},
  {path: 'assignment', canActivate: [AuthGuardService], component: AssignmentComponent},
  {path: 'exam', canActivate: [AuthGuardService], component: ExamComponent},
  {path: 'forecastAssignment', canActivate: [AuthGuardService], component: ForecastAssignComponent }, 
  {path: 'not-found', component: NotfoundComponent},
  {path: 'courseInfo', component: CourseinfoComponent} 
  // {path: '**', redirectTo: '/not-found'}
  // {path: '', redirectTo: '/login', pathMatch: 'full'},
  // {path: 'login', component: LoginComponent},
  // {path: 'main', component: MainComponent},
  // {path: 'admin', component: AdminComponent},
  // {path: 'module', component: ModuleComponent},
  // {path: 'staff', component: StaffComponent},
  // {path: 'addstaff', component: AddstaffComponent},
  // {path: 'staffinfo', component: StaffinfoComponent},
  // {path: 'staffreq', component: StaffreqComponent},
  // {path: 'staffpass', component: StaffpassComponent},
  // {path: 'maintenance', component: MaintenanceComponent},
  // {path: 'assignment', component: AssignmentComponent},
  // {path: 'exam', component: ExamComponent},
  // {path: 'not-found', component: NotfoundComponent},
  // {path: 'forecastAssignment', component: ForecastAssignComponent }, 
  // {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
