import { Directive, ElementRef, Renderer2, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appNavanimation]'
})
export class NavanimationDirective {

  constructor(public elRef: ElementRef, public renderer: Renderer2) { }

  @HostBinding('style.transform') transform: string

  @HostBinding('style.transition') transition: string

  @HostBinding('style.border-bottom') bottom: string

  @HostBinding('style.box-shadow') shadow: string

  @HostBinding('style.color') color: string

  ngOnInit() {
    this.bottom = '7px solid #404040'
  }

  @HostListener ('mouseenter') mouseOver(eventData: Event) {
    this.bottom = '7px solid #f83d5b'
    this.shadow = '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
    this.color = '#f83d5b'
  }

  @HostListener ('mouseleave') mouseOut(eventData: Event) {
    this.bottom = '7px solid #404040'
    this.shadow = 'none'
    this.color = 'black'
  }

}
