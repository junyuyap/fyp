import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment"
import { map } from "rxjs/operators";

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

export interface Levels {
  Staff: string;
  Level: number
}

export interface Auth {
  staff: string;
  password: string;
  level: number;
}

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class StaffinfoService {

  constructor(public httpClient: HttpClient) { }

  staffList: Info[] = [];

  sectionList: Section[] = [];

  levelsList: Levels[] = [];

  staffListUpdate = new EventEmitter<void>();

  newStaff = new EventEmitter<void>();

  constraintListUpdate = new EventEmitter<void>();

  newAuth: Auth = { staff: "", password: "", level: 0 };

  loadStaff() {
    return this.httpClient.get<Info[]>(`${APIEndpoint}/api/staff/list`).pipe(
      map((List) => {
        this.staffList = List;
        console.log("test");
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  loadSection() {
    return this.httpClient.get<Section[]>(`${APIEndpoint}/api/section/list`).pipe(
      map((List) => {
        this.sectionList = List;
        console.log("test");
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  loadLevels() {
    return this.httpClient.get<Levels[]>(`${APIEndpoint}/api/staff/levels`).pipe(
      map((List) => {
        this.levelsList = List;
        console.log("test");
        return List;
      },
        (error) => { console.log(error); })
    );
  }


  getStaff() {
    return this.staffList.slice();
  }

  getSection() {
    return this.sectionList.slice();
  }

  removeStaff(staffNo) {
    this.httpClient.delete<{ success: boolean }>
      (`${APIEndpoint}/api/staff/${staffNo}`)
      .subscribe((respond) => {
        this.staffListUpdate.emit();
      });
  }

  addStaff(newStaffInfo) {
    this.httpClient.put<Info>
      (`${APIEndpoint}/api/staff/${newStaffInfo.StaffNo}`, { info: newStaffInfo })
      .subscribe((respond) => {
        this.staffList.push(respond);
        this.staffListUpdate.emit();
      });
  }

  register(newStaffInfo) {
    this.httpClient.post<Auth>
      (`${APIEndpoint}/api/register/`, { "staff": newStaffInfo.staff, "password": newStaffInfo.password, "level": newStaffInfo.level })
      .subscribe((respond) => {
        console.log(respond);
      });
  }
  
  updateStaff(updateInfo: Info, temp) {
    console.log(temp)
    console.log(updateInfo);
    return this.httpClient.patch<{ success: boolean }>
      (`${APIEndpoint}/api/staff/${temp}`, { info: updateInfo })
      .pipe(map(
        (result) => {
          this.staffListUpdate.emit();
          return (result.success == true);
        },
        (error) => {
          console.log(error);
          return false;
        }
      ));
  }

}


