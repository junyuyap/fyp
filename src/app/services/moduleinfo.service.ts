import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment"
import { map } from "rxjs/operators";


const APIEndpoint = environment.APIEndpoint;

export interface Module {
  ModuleCode: string;
  Type: string;
  ModuleAbbrv: string;
  ModuleName: string;
  MassLecture: string;
  OddWeekLectHr: number;
  EvenWeekLectHr: number;
  OddWeekTutHr: number;
  EvenWeekTutHr: number;
  OddWeekPractHr: number;
  EvenWeekPractHr: number;
  MC: string;
  Moderator
}

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface ModReq {
  TeachingType: string;
  Requirements: string;
  Staff: string;
  Module: string;
}

export interface ModWeight {
    GroupSize: number;
    StartWk: number;
    EndWk: 5;
    Remarks: string;
    Module: string;
    AssmComCode: string;
    Weightage: number;
    NRC: boolean;
    Sequence: number;
}

@Injectable({
  providedIn: 'root'
})
export class ModuleinfoService {

  constructor(private httpClient: HttpClient) { }

  moduleList: Module[];

  modReqList: ModReq[];

  delReq: ModReq;

  requirement: ModReq;

  temp = { "Staff": "", "Module": "" }

  modReqUpdated = new EventEmitter<void>();

  modWeightage: [] = [];

  loadModules() {
    return this.httpClient.get<Module[]>(`${APIEndpoint}/api/module/list`).pipe(
      map((List) => {
        this.moduleList = List;
        console.log("test");
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  addModuleReq(newModReq) {
    this.requirement = newModReq
    this.temp.Staff = this.requirement.Staff
    this.temp.Module = this.requirement.Module
    console.log(JSON.stringify({
      info: {
        "TeachingType": this.requirement.TeachingType, "Requirements": this.requirement.Requirements,
        "Staff": this.requirement.Staff
      }
    }));
    this.httpClient.post<any>
      (`${APIEndpoint}/api/module-requirements/${this.requirement.Module}`, {
        info: {
          "TeachingType": this.requirement.TeachingType, "Requirements": this.requirement.Requirements,
          "Staff": this.requirement.Staff
        }
      })
      .subscribe((respond) => {
        this.modReqUpdated.emit();
        console.log(respond)
      });
  }

  delModReq(modReq) {
    this.delReq = modReq
    this.httpClient.delete<{ success: boolean }>
    (`${APIEndpoint}/api/module-requirements/${this.delReq.Module}/${this.delReq.Staff}?type=${this.delReq.TeachingType}`)
    .subscribe((respond) => {
      console.log(respond);
      this.modReqUpdated.emit();
    });
  }

  loadModReq(mod, staff) {
    return this.httpClient.get<ModReq[]>(`${APIEndpoint}/api/module-requirements/${mod}/${staff}`).pipe(
      map((List) => {
        console.log("test");
        this.modReqList = List
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  getModReq() {
    return this.modReqList.slice();
  }

  loadModWeightage(mod) {
    return this.httpClient.get<any>(`${APIEndpoint}/api/mod-weightage/${mod}`).pipe(
      map((List) => {
        console.log(List);
        this.modWeightage = List
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  updateWeightage(mod, updateInfo) {
    console.log(mod);
    return this.httpClient.patch<{ success: boolean }>
      (`${APIEndpoint}/api/mod-weightage/${mod}`, { info: updateInfo })
      .pipe(map(
        (result) => {
          return (result.success == true);
        },
        (error) => {
          console.log(error);
          return false;
        }
      ));
  }

  updateModReq(staff, mod, type, updateInfo) {
    console.log(updateInfo);
    const temp = {"Requirements": updateInfo}
    console.log(temp);
    return this.httpClient.patch<{ success: boolean }>
      (`${APIEndpoint}/api/module-requirements/${mod}/${staff}?type=${type}`, { info: temp})
      .pipe(map(
        (result) => {          
          this.modReqUpdated.emit();
          return (result.success == true)
        },
        (error) => {
          console.log(error);
          return false;
        }
      ));
  }
}
