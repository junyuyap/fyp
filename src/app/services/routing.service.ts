import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  active = new EventEmitter<void>();

  route: string = ""

  constructor() { }

  getActive(current) {
    this.route = current

  }

  sendActive() {
    this.active.emit();
    return this.route;

  }
}
