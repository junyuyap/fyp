import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment"
import { map } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";

// Data needed, Forecast, ModuleCourse, Teaching Assignment
// AcadYear Sem?


// add assignments by ModCourse Selected



// need to save modcourse data > Modcoord, remarks, assignments
// Patch module coordinator value on save. Not sure abt updateMC interface
// moduleinfo minimally requires ModuleCode
// updateModuleCord(moduleInfo: updateMC) {
//   const moduleCode = moduleInfo.ModuleCode;
//   console.log(moduleInfo);
//   return this.httpClient.patch<{ success: boolean }>
//     (`${APIEndpoint} /api/module/${moduleCode}`, { info: moduleInfo })
//     .pipe(map(
//       (result) => {
//         return (result.success == true);
//       },
//       (error) => {
//         console.log(error);
//         return false;
//       }
//     ));
// }

//  Forecast Assignment
// get the forecast assignment of the modcourse selected


// addForecastAssignment(forecastInfo: ForecastAssignment) {
//   const Course = forecastInfo.Course
//   const Module = forecastInfo.Module
//   this.httpClient.put<ForecastAssignment>
//   console.log(forecastInfo);
//   (`${APIEndpoint}/api/forecast/${Course}/${Module}?acadYear=<acadYear>&sem=<sem>`, { info: forecastInfo})
//   .subscribe((respond) => {
//     this.forecastAssignment.push(respond);
//     this.forecastAssigmentUpdate.emit();
//   });
// }

// updateForecastAssignment(forecastInfo: ForecastAssignment) {
//   const Course = forecastInfo.Course
//   const Module = forecastInfo.Module
//   console.log(forecastInfo);
//   return this.httpClient.patch<{ success: boolean }>
//     (`${APIEndpoint}/api/forecast/${Course}/${Module}?acadYear=<acadYear>&sem=<sem>`, { info: forecastInfo})
//     .pipe(map(
//       (result) => {
//         return (result.success == true);
//       },
//       (error) => {
//         console.log(error);
//         return false;
//       }
//     ));
// }



export interface AcadYearSem {
  Year: string,
  Sem: number
}

export interface ForecastAssignment {
  TotalLectClass: number,
  TotalTutClass: number,
  TotalPractClass: number,
  StuStageQty: number,
  Module: string,
  Course: string,
  Year: string,
  Sem: number
}

export interface ModCourse {
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string
}


export interface Assignment {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  Staff: string,
  Module: string,
  Course: string
}

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

export interface Module {
  ModuleCode: string;
  Type: number;
  ModuleAbbrv: string;
  ModuleName: string;
  MassLecture: string;
  OddWeekLectHr: number;
  EvenWeekLectHr: number;
  OddWeekTutHr: number;
  EvenWeekTutHr: number;
  OddWeekPractHr: number;
  EvenWeekPractHr: number;
  MC: string;
  Moderator: string;
}

 export interface Course {
   CourseAbbrv: string,
   CourseName: string
 }

 export interface AcadYearObj{
   Year: string,
   Sem: number
 }

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class TasService {

  private selectedSource = new BehaviorSubject<number>(1);
  currentSelected = this.selectedSource.asObservable();

  private givenModKey = new BehaviorSubject<object>({})
  currentModKey = this.givenModKey.asObservable();

  private selectedYearSem = new BehaviorSubject<AcadYearObj>({Year: "1", Sem: 2})
  currentYearSem = this.selectedYearSem.asObservable();

  constructor(public httpClient: HttpClient) { }

  staffList: Info[] = [];

  sectionList: Section[] = [];

  moduleList: Module[] = [];

  modCourseList: ModCourse[] = [];

  assignmentList: Assignment[] = [];

  forecastAssignment: ForecastAssignment;

  acadYearSem: AcadYearSem[] = []

  assignmentListUpdate = new EventEmitter<void>();

  courseList: Course[] = [];

  selectedMC;

  modCourse: ModCourse;

  changeSelected(selectedValue: number) {
    this.selectedSource.next(selectedValue)
  }

  changeModKey(newModKey) {
    this.givenModKey.next(newModKey)
  }

  changeYearSem(YS) {
    this.selectedYearSem.next(YS)
  }

  loadModule() {
    return this.httpClient.get<Module[]>(`${APIEndpoint}/api/module/list`).pipe(
      map((List) => {
        this.moduleList = List;
        return List;
      },
        (error) => { console.log(error); })
    )
  }
  

  loadStaff() {
    return this.httpClient.get<Info[]>(`${APIEndpoint}/api/staff/list`).pipe(
      map((List) => {
        this.staffList = List;
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  loadSection() {
    return this.httpClient.get<Section[]>(`${APIEndpoint}/api/section/list`).pipe(
      map((List) => {
        this.sectionList = List;
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  // get modcourse data
  loadModCourse() {
    return this.httpClient.get<ModCourse[]>(`${APIEndpoint}/api/modcourse/list`).pipe(
      map((List) => {
        this.modCourseList = List;
        return List;
      },
        (error) => { console.log(error); })
    )
  }

  loadCourse() {
    return this.httpClient.get<Course[]>(`${APIEndpoint}/api/course/list`).pipe(
      map((List) => {
        this.courseList = List;
        return List;
      },
        (error) => { console.log(error); })
    )
  }

  // 
  loadAcadYearSem() {
    return this.httpClient.get<AcadYearSem[]>(`${APIEndpoint}/api/academic-year/list`).pipe(
      map((List) => {
        this.acadYearSem = List;
        return List;
      },
        (error) => { console.log(error); })
    )
  }

  // get all assignments by modules !!!!DONT FORGET TO CHANGE ACAD YEAR OR SEM!!!!
  loadAssignmentByModule(object, acadYearSem) {
    return this.httpClient.get<Assignment[]>(`${APIEndpoint}/api/assignment/${object.Course}/${object.ModuleID}?acadYear=${acadYearSem.Year}&sem=${acadYearSem.Sem}`).pipe(
      map((List) => {
        this.assignmentList = List;
        return List;
      },
        (error) => { console.log(error); })
    )
  }
  
  loadAssignmentByStaff(staffNo, acadYearSem) {
    return this.httpClient.get<Assignment[]>(`${APIEndpoint}/api/assignment/${staffNo}?acadYear=${acadYearSem.Year}&sem=${acadYearSem.Sem}`).pipe(
      map((List) => {
        this.assignmentList = List;
        return List;
      },
        (error) => { console.log(error); })
    )
  }

  addAssignment(Course, Module, AssignmentInfo, acadYearSem) {
    this.httpClient.put<Assignment>
      (`${APIEndpoint}/api/assignment/${Course}/${Module}?acadYear=${acadYearSem.Year}&sem=${acadYearSem.Sem}`, { info: AssignmentInfo })
      .subscribe((respond) => {
        this.assignmentList.push(respond);
        this.assignmentListUpdate.emit();
        console.log("Added to Assignment", Course, Module, "with Info ", AssignmentInfo);
        alert("Successfully Added New Assignment")
      })
  }

  removeAssignment(Module, Course, StaffNo, acadYearSem) {
    this.httpClient.delete<{ success: boolean }>
      (`${APIEndpoint}/api/assignment/${Course}/${Module}/${StaffNo}?acadYear=${acadYearSem.Year}&sem=${acadYearSem.Sem}`)
      .subscribe((respond) => {
      });
  }

  loadForecastAssignment(Course, Module, acadYearSem) {
    return this.httpClient.get<ForecastAssignment>(`${APIEndpoint}/api/forecast/${Course}/${Module}?acadYear=${acadYearSem.Year}&sem=${acadYearSem.Sem}`).pipe(
      map((List) => {
       this.forecastAssignment = List;
       return List;
      },
     (error) => { console.log(error); })
   )
  }

  addForecastAssignment(forecastInfo: ForecastAssignment, acadYearSem) {
  const Course = forecastInfo.Course
  const Module = forecastInfo.Module
  this.httpClient.put<ForecastAssignment>
  (`${APIEndpoint}/api/forecast/${Course}/${Module}?acadYear=${acadYearSem.Year}>&sem=${acadYearSem.Sem}`, { info: forecastInfo})
  .subscribe((respond) => {
    
    
  });
}

  addModule(moduleInfo) {
    this.httpClient.put<Module>(`${APIEndpoint}/api/module`, { info: moduleInfo })
    .subscribe((respond) => {
      alert("Sucessfully added a new Module")
    });
  }

  addModuleCourse(info) {
    this.httpClient.put<ModCourse>(`${APIEndpoint}/api/modcourse`, { info: info })
    .subscribe((respond) => {
      alert("Sucessfully added a new Module")
    });
  }

  updateForecastAssignment(forecastInfo, acadYearSem) {
    const Course = forecastInfo.Course
    const Module = forecastInfo.Module
    const forecastData = {
      TotalLectClass: forecastInfo.TotalLectClass,
      TotalTutClass: forecastInfo.TotalTutClass,
      TotalPractClass: forecastInfo.TotalPractClass,
      StuStageQty: forecastInfo.StuStageQty
    }

    return this.httpClient.patch<{ success: boolean }>
      (`${APIEndpoint}/api/forecast/${Course}/${Module}?acadYear=${acadYearSem.Year}&sem=${acadYearSem.Sem}`, { info: forecastData })
      .pipe(map(
        (result) => {
          return (result.success == true);
        },
        (error) => {
          console.log(error);
          return false;
        }
      ));
  }

  // update MC info obj 
  updateMC(Module, staff) {
    const info = {
      MC: staff
    }

    return this.httpClient.patch<{ success: boolean }>
    (`${APIEndpoint}/api/module/${Module}`, { info: info })
    .pipe(map(
      (result) => {
        return (result.success == true);
      },
      (error) => {
        console.log(error);
        return false;
      }
    ));
  }

  updateAssignment(info, course, module, YearSem) {
    const assignment = {
      NoLectClass: info.NoLectClass,
      NoTutClass: info.NoTutClass,
      NoPractClass: info.NoPractClass
    }

    return this.httpClient.patch<{ success: boolean }>
    (`${APIEndpoint}/api/assignment/${course}/${module}/${info.Staff}?acadYear=${YearSem.Year}&sem=${YearSem.Sem}`, { info: assignment })
    .pipe(map(
      (result) => {
        alert("Successfully Added updated Assignment");
        return (result.success == true);
      },
      (error) => {
        console.log(error);
        return false;
      }
    ));
  }

// /api/modcourse/:course/:moduleCode
  getSpecificModCourse(Course, Module, acadYearSem) {
    return this.httpClient.get<ModCourse>(`${APIEndpoint}/api/modcourse/${Course}/${Module}`).pipe(
      map((List) => {
       this.modCourse = List;
       return List;
      },
     (error) => { console.log(error); })
   )
  }

  removeCourse(Course) {
    this.httpClient.delete<{ success: boolean }>
      (`${APIEndpoint}/api/course/${Course}`)
      .subscribe((respond) => {
        console.log("deleted")
      });
  }

  addCourse(course) {
    this.httpClient.post<Course>(`${APIEndpoint}/api/course`, { info: course })
    .subscribe((respond) => {
      alert("Sucessfully added a new course");
    });
  }

  getStaff() {
    return this.staffList.slice();
  }

  getSection() {
    return this.sectionList.slice();
  }

  getModule() {
    return this.moduleList.slice();
  }

  getModCourse() {
    return this.modCourseList.slice();
  }


}
