import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment"
import { map } from "rxjs/operators";

export interface Exam {
  ChiefExaminer: string;
  CoExaminer: string;
  Moderator: string;
  ExternalModeration: string;
  MDEO: string;
  Marker: string;
  Verifier: string;
  CoVerifier: string;
  VerifierDetails: string;
  MarksModerator: string;
  SharePaper: string;
  ShareQuestion: string;
  Module: string;
  Course: string;
}


const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class ExaminfoService {

  examList: Exam[] = [];

  constructor(private httpClient: HttpClient) { }

  loadExam(course) {
    return this.httpClient.get<Exam[]>(`${APIEndpoint}/api/exam/${course}`).pipe(
      map((List) => {
        this.examList = List;
        console.log("test");
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  updateExam(course, mod, chief, co, moderator, exmod, mdeo, marker, verifier, coverifier, verifierdetails, marksmod, sharep, shareq) {
    console.log( {
      info: {
        "ChiefExaminer": chief,
        "CoExaminer": co,
        "Moderator": moderator,
        "ExternalModeration": exmod,
        "MDEO": mdeo,
        "Marker": marker,
        "Verifier": verifier,
        "CoVerifier": coverifier,
        "VerifierDetails": verifierdetails,
        "MarksModerator": marksmod,
        "SharePaper": sharep,
        "ShareQuestion": shareq
      }
    })
    return this.httpClient.patch<{ success: boolean }>
      (`${APIEndpoint}/api/exam/${course}/${mod}`, {
        info: {
          "ChiefExaminer": chief,
          "CoExaminer": co,
          "Moderator": moderator,
          "ExternalModeration": exmod,
          "MDEO": mdeo,
          "Marker": marker,
          "Verifier": verifier,
          "CoVerifier": coverifier,
          "VerifierDetails": verifierdetails,
          "MarksModerator": marksmod,
          "SharePaper": sharep,
          "ShareQuestion": shareq
        }
      })
      .pipe(map(
        (result) => {
          return (result.success == true);
        },
        (error) => {
          console.log(error);
          return false;
        }
      ));
  }
}
