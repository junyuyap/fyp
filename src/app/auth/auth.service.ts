import { Injectable, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment"
import { map } from "rxjs/operators";
import { StaffinfoService } from "../services/staffinfo.service";

const APIEndpoint = environment.APIEndpoint;

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Constraint {
  Others: string;
  Day: string;
  Start: string;
  End: string;
  Reason: string;
  Staff: string;
}

export interface Auth {
  staff: string;
  password: string;
  level: number;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedIn = false;

  status: any = {};

  staffInfo: Info;

  staffList: Info[];

  StaffConstraint: Constraint;

  constraintListUpdate = new EventEmitter<void>();

  newAuth: Auth = { staff: "", password: "", level: 0 };

  particulars: Auth = { staff: "", password: "", level: 0 };

  constraintList: Constraint[] = [];

  constructor(private router: Router, public httpClient: HttpClient, private staffinfo: StaffinfoService) { }

  login(username: string, password: string) {

    this.particulars.password = password
    this.particulars.staff = username;
    console.log(this.particulars);

    this.httpClient.post<any>
      (`${APIEndpoint}/api/login/`, { "staff": username, "password": password })
      .subscribe((respond) => {
        this.status = respond
        console.log(this.status);
        if (this.status.message == "success") {
          this.loggedIn = true;
          this.router.navigate(["/main"]);
          this.httpClient.get<Info>(`${APIEndpoint}/api/staff/${this.particulars.staff}`).subscribe
            ((result) => {
              this.staffInfo = result;
              console.log(this.staffInfo);
              return result;
            },
              (error) => { console.log(error); })
        }
        else {
          alert(this.status.message);
        }
      });

  }

  loadConstraint() {
    return this.httpClient.get<Constraint[]>(`${APIEndpoint}/api/staff-constraint/${this.particulars.staff}`).pipe(
      map((List) => {
        this.constraintList = List;
        console.log(this.constraintList);
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  loadAcadYear() {
    return this.httpClient.get<any>(`${APIEndpoint}/api/academic-year/list`).pipe(
      map((List) => {
        console.log(List);
        return List;
      },
        (error) => { console.log(error); })
    );
  }

  addContraint(newConstraint) {
    this.StaffConstraint = newConstraint
    console.log(this.StaffConstraint);
    this.httpClient.post<Constraint>
      (`${APIEndpoint}/api/staff-constraint/add`, {
        constraint: newConstraint
      })
      .subscribe((respond) => {
        console.log(respond)
        this.constraintListUpdate.emit();
      });
  }

  delConstraint(constraintID) {
    this.httpClient.delete<{ success: boolean }>
      (`${APIEndpoint}/api/staff-constraint/${constraintID}`)
      .subscribe((respond) => {
        this.constraintListUpdate.emit();
      });
  }

  updatePass(info, update) {
    console.log(info.StaffNo);
    return this.httpClient.patch<{ success: boolean }>
      (`${APIEndpoint}/api/${info.StaffNo}`, update )
      .pipe(map(
        (result) => {
          alert('Password updated');
          return (result.success == true);
        },
        (error) => {
          console.log(error);
          alert(error);
          return false;
        }
      ));
  }

  register(authInfo) {
    this.newAuth = authInfo
    console.log(this.newAuth);
    this.httpClient.post<Auth>
      (`${APIEndpoint}/api/register/`, { "staff": this.newAuth.staff, "password": this.newAuth.password, "level": this.newAuth.level })
      .subscribe((respond) => {
        console.log(respond);
      });
  }

  logout() {
    this.loggedIn = false;
    this.router.navigate(["/"]);
  }

  getLevel() {
    console.log(this.status);
    return this.status.Level;
  }

  getPass() {
    return this.particulars.password
  }

  getStaff() {

    this.staffinfo.loadStaff().subscribe((result) => {
      this.staffList = result;
      for (let staff of this.staffList) {
        if (this.particulars.staff == staff.StaffNo) {
          this.staffInfo = staff;
          console.log(this.staffInfo);
        }
      }
    })
    return this.staffInfo;
  }

  isAuthenticated() {
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          resolve(this.loggedIn)
        }, 1000)
      }
    );
    return promise
  }

  updateConstraint(updateInfo, id) {
    console.log(updateInfo);
    return this.httpClient.patch<{ success: boolean }>
      (`${APIEndpoint}/api/staff-constraint/${id}`, { info: updateInfo })
      .pipe(map(
        (result) => {
          this.constraintListUpdate.emit();
          return (result.success == true);
        },
        (error) => {
          console.log(error);
          return false;
        }
      ));
  }
}
