import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { RoutingService } from "../../services/routing.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { StaffinfoService } from "../../services/staffinfo.service";
import { AuthService } from "../../auth/auth.service";

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

@Component({
  selector: 'app-staffinfo',
  templateUrl: './staffinfo.component.html',
  styleUrls: ['./staffinfo.component.css']
})
export class StaffinfoComponent implements OnInit {

  constructor(public router: Router, private routing: RoutingService, private staffinfo: StaffinfoService, private auth: AuthService) { }

  personalInfo: Info = {
    StaffNo: "", StaffName: "", StaffType: "", Honorific: "", Designation: "", StaffEmail: "",
    StaffContactNo: "", StaffHPNo: "", StaffAbbrv: "", Remarks: "", Section: 0
  };

  updateInfo: Info;

  staffList: Info[];

  updatePersonal: FormGroup;

  ngOnInit() {

    this.personalInfo = this.auth.getStaff();

    this.updatePersonal = new FormGroup({
      'StaffName': new FormControl(this.personalInfo.StaffName, [Validators.required]),
      'StaffType': new FormControl(this.personalInfo.StaffType, [Validators.required]),
      'Honorific': new FormControl(this.personalInfo.Honorific, [Validators.required]),
      'Designation': new FormControl(this.personalInfo.Designation, [Validators.required]),
      'StaffEmail': new FormControl(this.personalInfo.StaffEmail, [Validators.required]),
      'StaffContactNo': new FormControl(this.personalInfo.StaffContactNo, [Validators.required]),
      'StaffHPNo': new FormControl(this.personalInfo.StaffHPNo, [Validators.required]),
      'StaffAbbrv': new FormControl(this.personalInfo.StaffAbbrv, [Validators.required]),
    })

    console.log(this.personalInfo);

  }

  onSubmit() {

    this.updateInfo = {
      StaffNo: this.personalInfo.StaffNo, StaffName: this.updatePersonal.value.StaffName, StaffType: this.updatePersonal.value.StaffType,
      Honorific: this.updatePersonal.value.Honorific, Designation: this.updatePersonal.value.Designation, StaffEmail: this.updatePersonal.value.StaffEmail,
      StaffContactNo: this.updatePersonal.value.StaffContactNo, StaffHPNo: this.updatePersonal.value.StaffHPNo, StaffAbbrv: this.updatePersonal.value.StaffAbbrv,
      Remarks: this.personalInfo.Remarks, Section: this.personalInfo.Section
    };

    console.log(this.updateInfo);

    this.staffinfo.updateStaff(this.updateInfo, this.updateInfo.StaffNo).subscribe();

    if (this.updatePersonal.valid) {
      alert('Information successfully updated');
    }
    else {
      alert("All fields are required");
    }
  }

}
