import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { Router } from "@angular/router";
import { RoutingService } from "../../../services/routing.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../../auth/auth.service";

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Auth {
  staff: string;
  password: string;
  level: number;
}


@Component({
  selector: 'app-staffpass',
  templateUrl: './staffpass.component.html',
  styleUrls: ['./staffpass.component.css']
})
export class StaffpassComponent implements OnInit {

  updatePass: FormGroup;

  oldpass;

  personalInfo: Info = {
    StaffNo: "", StaffName: "", StaffType: "", Honorific: "", Designation: "", StaffEmail: "",
    StaffContactNo: "", StaffHPNo: "", StaffAbbrv: "", Remarks: "", Section: 0
  };

  constructor(public router: Router, private routing: RoutingService, private auth: AuthService) { }

  ngOnInit() {

    this.personalInfo = this.auth.getStaff();
    this.oldpass = this.auth.getPass()
    this.updatePass = new FormGroup({
      'oldPassword': new FormControl(null, [Validators.required]),
      'newPassword': new FormControl(null, [Validators.required])
    })
  }

  onSubmit() {
    if (this.oldpass !== this.updatePass.value.oldPassword) {
      alert('Your old password is incorrect')
    }
    else {
      console.log(this.updatePass.value.newPassword);
      this.auth.updatePass(this.personalInfo, {"password": this.updatePass.value.newPassword}).subscribe();
      this.personalInfo = this.auth.getStaff();
      this.oldpass = this.auth.getPass()
      this.updatePass.reset()
    }
  }

}
