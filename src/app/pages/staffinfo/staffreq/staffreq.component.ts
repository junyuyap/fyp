import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { RoutingService } from "../../../services/routing.service";
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../../auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment"

const APIEndpoint = environment.APIEndpoint;

export interface Constraint {
  Others: string;
  Day: string;
  Start: string;
  End: string;
  Reason: string;
  Staff: string;
}

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

@Component({
  selector: 'app-staffreq',
  templateUrl: './staffreq.component.html',
  styleUrls: ['./staffreq.component.css']
})
export class StaffreqComponent implements OnInit {

  constructor(public router: Router, private routing: RoutingService, private auth: AuthService, private httpClient: HttpClient) { }

  displayedColumns: string[] = ['day', 'start', 'end', 'reason', 'action'];

  newRequirement: FormGroup;

  updateRequirement: FormGroup;

  personalInfo: Info = {
    StaffNo: "", StaffName: "", StaffType: "", Honorific: "", Designation: "", StaffEmail: "",
    StaffContactNo: "", StaffHPNo: "", StaffAbbrv: "", Remarks: "", Section: 0
  };

  personalContraint: Constraint;

  constraintList: Constraint[] = [];

  data = new MatTableDataSource<Constraint>();

  tempConstraint: Constraint;

  newConstraint = {};

  selected;

  ngOnInit() {

    this.personalInfo = this.auth.getStaff();

    this.auth.loadConstraint().subscribe((result) => {
      this.constraintList = result
      for (var index in result) {
        this.constraintList[index].Start = result[index].Start.substring(0, 5)
        this.constraintList[index].End = result[index].End.substring(0, 5)
      }
      this.data = new MatTableDataSource<Constraint>(this.constraintList);
    })

    this.newRequirement = new FormGroup({
      'Others': new FormControl(null, [Validators.required]),
      'Day': new FormControl(null, [Validators.required]),
      'Start': new FormControl(null, [Validators.required]),
      'End': new FormControl(null, [Validators.required]),
      'Reason': new FormControl(null, [Validators.required])
    });

    this.updateRequirement = new FormGroup({
      'uOthers': new FormControl(null, [Validators.required]),
      'uDay': new FormControl(null, [Validators.required]),
      'uStart': new FormControl(null, [Validators.required]),
      'uEnd': new FormControl(null, [Validators.required]),
      'uReason': new FormControl(null, [Validators.required])
    });

  }

  addReq() {
    console.log(this.personalInfo);
    this.personalContraint = {
      "Others": this.newRequirement.value.Others,
      "Day": this.newRequirement.value.Day,
      "Start": this.newRequirement.value.Start + "00",
      "End": this.newRequirement.value.End + "00",
      "Reason": this.newRequirement.value.Reason,
      "Staff": this.personalInfo.StaffNo
    }
    console.log(this.personalContraint);

    if (this.newRequirement.valid) {
      this.auth.addContraint(this.personalContraint);
      this.newRequirement.reset();
      alert('Constraint added successfully');
    }
    else {
      alert('One or more of the fields are invalid');
    }

    this.auth.constraintListUpdate.subscribe(() => {
      this.auth.loadConstraint().subscribe((result) => {
        this.constraintList = result
        console.log(this.constraintList);
        for (var index in result) {
          this.constraintList[index].Start = result[index].Start.substring(0, 5)
          this.constraintList[index].End = result[index].End.substring(0, 5)
        }
        this.data = new MatTableDataSource<Constraint>(this.constraintList);
      })
    })
  }

  deleteConstraint(constraintID) {
    console.log(constraintID);
    this.auth.delConstraint(constraintID);
    alert('Constraint has been deleted successfully');
    this.auth.constraintListUpdate.subscribe(() => {
      this.auth.loadConstraint().subscribe((result) => {
        this.constraintList = result
        for (var index in result) {
          this.constraintList[index].Start = result[index].Start.substring(0, 5)
          this.constraintList[index].End = result[index].End.substring(0, 5)
        }
        this.data = new MatTableDataSource<Constraint>(this.constraintList);
      })
    });
  }

  onUpdate() {
    this.newConstraint = {
      "Others": this.updateRequirement.value.uOthers,
      "Day": this.updateRequirement.value.uDay,
      "Start": this.updateRequirement.value.uStart,
      "End": this.updateRequirement.value.uEnd,
      "Reason": this.updateRequirement.value.uReason
    }
    if (this.updateRequirement.valid) {
      this.auth.updateConstraint(this.newConstraint, this.selected).subscribe();
      this.auth.constraintListUpdate.subscribe(() => {
        this.auth.loadConstraint().subscribe((result) => {
          this.constraintList = result
          for (var index in result) {
            this.constraintList[index].Start = result[index].Start.substring(0, 5)
            this.constraintList[index].End = result[index].End.substring(0, 5)
          }
          this.data = new MatTableDataSource<Constraint>(this.constraintList);
          alert("Update Success");
        })
      })
    }
    else {
      alert('One or more of the fields are invalid');
    }
  }

  updateConstraint(info, id) {
    this.tempConstraint = info;
    this.selected = id;
    this.updateRequirement = new FormGroup({
      'uOthers': new FormControl(this.tempConstraint.Others, [Validators.required]),
      'uDay': new FormControl(this.tempConstraint.Day, [Validators.required]),
      'uStart': new FormControl(this.tempConstraint.Start, [Validators.required]),
      'uEnd': new FormControl(this.tempConstraint.End, [Validators.required]),
      'uReason': new FormControl(this.tempConstraint.Reason, [Validators.required])
    });
  }
}
