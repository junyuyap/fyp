import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../auth/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor( private router: Router, private auth: AuthService) { }

  staffLevel : number;

  ngOnInit() {
  }

  check() {
    this.staffLevel = this.auth.getLevel();
    console.log(this.staffLevel);
    if (this.staffLevel != 5) {
      alert("You have no access to this page");
      this.router.navigate(["/main"]);
    }
  }

}
