import { Component, OnInit } from '@angular/core';
import { ModuleinfoService } from "../../services/moduleinfo.service";
import { ExaminfoService } from "../../services/examinfo.service";
import { StaffinfoService } from "../../services/staffinfo.service";

export interface Exam {
  ChiefExaminer: string;
  CoExaminer: string;
  Moderator: string;
  ExternalModeration: string;
  MDEO: string;
  Marker: string;
  Verifier: string;
  CoVerifier: string;
  VerifierDetails: string;
  MarksModerator: string;
  SharePaper: string;
  ShareQuestion: string;
  Module: string;
  Course: string;
}

export interface Module {
  ModuleCode: string;
  Type: string;
  ModuleAbbrv: string;
  ModuleName: string;
  MassLecture: string;
  OddWeekLectHr: number;
  EvenWeekLectHr: number;
  OddWeekTutHr: number;
  EvenWeekTutHr: number;
  OddWeekPractHr: number;
  EvenWeekPractHr: number;
  MC: string;
  Moderator
}

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})


export class ExamComponent implements OnInit {

  constructor(private modService: ModuleinfoService, private examService: ExaminfoService, private staffService: StaffinfoService) { }

  staffList: Info[] = [];

  examList: Exam[] = [];

  modList : Module[] = [];

  courseList = []

  type = {};

  mdeo = {};

  verifier = {};

  verifierdetails = {}

  marksmoderator = {}

  chiefexaminer = {}

  moderator = {}

  externalmoderation = {}

  course = {}

  module = {}

  coexaminer = {}

  marker = {}

  coverifier = {}

  sharepaper = {}

  sharequestion = {}

  test = 1;

  ngOnInit() {

    this.staffService.loadSection().subscribe((result) => {
      this.courseList = result;
    })

    this.modService.loadModules().subscribe((result) => {
      this.modList = result;
      console.log(this.modList);
    })

    this.examService.loadExam("DBIT").subscribe((result) => {
      this.examList = result
      for (let exam of this.examList) {
        this.mdeo[exam.Module] = exam.MDEO;
        this.moderator[exam.Module] = exam.Moderator;
        this.verifier[exam.Module] = exam.Verifier
        this.verifierdetails[exam.Module] = exam.VerifierDetails;
        this.marksmoderator[exam.Module] = exam.MarksModerator;
        this.externalmoderation[exam.Module] = exam.ExternalModeration;
        this.chiefexaminer[exam.Module] = exam.ChiefExaminer;

        this.modService.loadModules().subscribe((result) => {
          this.modList = result;
          for (let mod of this.modList) {
            if (mod.ModuleCode == exam.Module) {
              this.type[exam.Module] = mod.Type;
            }
          };
        });
      }
    })
    this.staffService.loadStaff().subscribe((result) => {
      this.staffList = result
    })
  }


  changeCourse(info) {
    this.examService.loadExam(info.target.value).subscribe((result) => {
      this.examList = result;
      console.log(this.examList);
    })
  }

  onSubmit(mod, course) {

    if (this.sharequestion == null) {
      this.sharequestion = null
    } else if (Object.keys(this.sharequestion).length == 0) {
      this.sharequestion = null
    }

    if (this.sharepaper == null) {
      this.sharepaper = null
    } else if (Object.keys(this.sharepaper).length == 0) {
      this.sharepaper = null
    }

    if (this.coexaminer == null) {
      this.coexaminer = null
    } else if (Object.keys(this.coexaminer).length == 0) {
      this.coexaminer = null
    }

    if (this.coverifier == null) {
      this.coverifier = null
    } else if (Object.keys(this.coverifier).length == 0) {
      this.coverifier= null
    }

    if (this.marker == null) {
      this.marker = null
    } else if (Object.keys(this.marker).length == 0) {
      this.marker = null
    }

    this.examService.updateExam(course, mod, this.chiefexaminer[mod], this.coexaminer, this.moderator[mod],
      this.externalmoderation[mod], this.mdeo[mod], this.marker, this.verifier[mod], this.coverifier, this.verifierdetails[mod],
      this.marksmoderator[mod], this.sharepaper, this.sharequestion).subscribe();

      alert("Exam information updated successfully");
  }
}
