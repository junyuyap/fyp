import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from "@angular/forms";
import { AuthService } from "../../auth/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  adminForm: FormGroup;

  submitted = false;

  yearSem = [];

  selected = ""

  constructor(public authService: AuthService) { }

  ngOnInit() {
    this.adminForm = new FormGroup({
      'username'   : new FormControl(null, [Validators.required, this.blankSpaces]),
      'userpassword'  : new FormControl(null, [Validators.required, this.blankSpaces])
      // 'yearsem': new FormControl(null, [Validators.required])
    });
    this.authService.loadAcadYear().subscribe((result) => {
      this.yearSem = result
    })
  }

  blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }

  onSubmit() {
    this.submitted = true;
    this.adminForm.reset();    
  }

  changeData(a) {
    console.log(a);
  }

  onLogin(username: HTMLInputElement, password: HTMLInputElement) {
    this.authService.login(username.value, password.value)
  }
}
