import { Component, OnInit } from '@angular/core';
import { StaffinfoService } from "../../services/staffinfo.service";
import { ModuleinfoService } from "../../services/moduleinfo.service";
import { AuthService } from "../../auth/auth.service";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatTableDataSource } from '@angular/material/table';

export interface Module {
  ModuleCode: string;
  Type: string;
  ModuleAbbrv: string;
  ModuleName: string;
  MassLecture: string;
  OddWeekLectHr: number;
  EvenWeekLectHr: number;
  OddWeekTutHr: number;
  EvenWeekTutHr: number;
  OddWeekPractHr: number;
  EvenWeekPractHr: number;
  MC: string;
}

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface ModReq {
  TeachingType: string;
  Requirements: string;
  Staff: string;
  Module: string;
}

export interface ModWeight {
  GroupSize: number;
  StartWk: number;
  EndWk: number;
  Remarks: string;
  Module: string;
  AssmComCode: string;
  Weightage: number;
  NRC: boolean;
  Sequence: number;
}


@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})

export class ModuleComponent implements OnInit {

  moduleList: Module[];

  mc: Info;

  modRequirement: ModReq;

  modRequirementList: ModReq[];

  mcMod: Module[] = [];

  mst = [];
  ca = [];

  newmst = {};
  newca = {};

  selectedMod: Module = {
    ModuleCode: "Select a module first",
    Type: "Select a module first",
    ModuleAbbrv: "Select a module first",
    ModuleName: "Select a module first",
    MassLecture: "",
    OddWeekLectHr: 0,
    EvenWeekLectHr: 0,
    OddWeekTutHr: 0,
    EvenWeekTutHr: 0,
    OddWeekPractHr: 0,
    EvenWeekPractHr: 0,
    MC: "Select a module first"
  };

  newModReq: FormGroup;

  newUpdateReq: FormGroup;

  data = new MatTableDataSource<ModReq>();

  modWeightage: [] = [];

  GroupSize = {};
  StartWk = {};
  EndWk = {};
  Remarks = {};
  Module = {};
  AssmComCode = {};
  Weightage = {};
  NRC = {};
  Sequence = {};
  Requirement = {};
  TestWkType = {};
  Type = {};
  Duration = {};

  tempReq: ModReq;


  displayedColumns: string[] = ['type', 'requirement', 'action'];

  constructor(private moduleService: ModuleinfoService, private staffInfoService: StaffinfoService, private auth: AuthService, private router: Router) { }

  ngOnInit() {

    this.newModReq = new FormGroup({
      'Type': new FormControl(null, [Validators.required]),
      'Requirement': new FormControl(null, [Validators.required])
    });

    
    this.newUpdateReq = new FormGroup({
      'Type': new FormControl(null, [Validators.required]),
      'Requirement': new FormControl(null, [Validators.required])
    });

    this.mc = this.auth.getStaff();
    this.moduleService.loadModules().subscribe((result) => {
      this.moduleList = result
      console.log(this.moduleList);
      for (let mod of this.moduleList) {
        if (this.mc.StaffNo == mod.MC) {
          this.mcMod.push(mod)
        }
      }
      if (this.mcMod.length === 0) {
        alert("You are not a module coordinator")
        this.router.navigate(['/main']);
      }
    });
  }

  changeMod(info) {
    this.ca = [];
    this.mst = [];
    for (let mod of this.mcMod) {
      if (mod.ModuleAbbrv == info.target.value) {
        this.selectedMod = mod;
        console.log(this.selectedMod.ModuleCode);
        this.moduleService.loadModReq(this.selectedMod.ModuleCode, this.mc.StaffNo).subscribe((result) => {
          this.data = new MatTableDataSource<ModReq>(result)
        })
        this.moduleService.loadModWeightage(this.selectedMod.ModuleCode).subscribe((result) => {
          this.modWeightage = result;
          for (let weight of result) {
            if (weight.AssmComCode == "MST" || weight.AssmComCode == "EST") {
              this.mst.push(weight);
              this.TestWkType[weight.AssmComCode] = weight.TestWkType
              this.Type[weight.AssmComCode] = weight.Type
              this.Duration[weight.AssmComCode] = weight.Duration
              this.Requirement[weight.AssmComCode] = weight.Requirement
            }
            else {
              this.ca.push(weight);
              this.GroupSize[weight.AssmComCode] = weight.GroupSize;
              this.StartWk[weight.AssmComCode] = weight.StartWk;
              this.EndWk[weight.AssmComCode] = weight.EndWk;
              this.Remarks[weight.AssmComCode] = weight.Remarks
            }
          }
          console.log(this.modWeightage);
        });
      }
    }
  }

  onSubmit() {
    this.modRequirement = {
      "TeachingType": this.newModReq.value.Type,
      "Requirements": this.newModReq.value.Requirement,
      "Staff": this.mc.StaffNo,
      "Module": this.selectedMod.ModuleCode
    }
    console.log(this.modRequirement);
    if (this.newModReq.valid) {
      this.moduleService.addModuleReq(this.modRequirement);
      this.moduleService.modReqUpdated.subscribe(() => {
        this.moduleService.loadModReq(this.selectedMod.ModuleCode, this.mc.StaffNo).subscribe((result) => {
          this.data = new MatTableDataSource<ModReq>(result)
        });
      });
      alert("Module Requirement successfully added")
      this.newModReq.reset();
    }
    else {
      alert("All fields are required")
    }

  }

  delReq(del) {
    console.log(del)
    this.moduleService.delModReq(del);
    this.moduleService.modReqUpdated.subscribe(() => {
      this.moduleService.loadModReq(this.selectedMod.ModuleCode, this.mc.StaffNo).subscribe((result) => {
        this.data = new MatTableDataSource<ModReq>(result)
      });
    });
    alert("Module Requirement has been deleted")
  }

  updateWeight() {

    const temp = [];

    for (let modweightmst of this.mst) {
      temp.push({
        "TestWkType": this.TestWkType[modweightmst.AssmComCode], "Type": this.Type[modweightmst.AssmComCode],
        "Duration": this.Duration[modweightmst.AssmComCode], "Requirement": this.Requirement[modweightmst.AssmComCode],
        "Module": modweightmst.Module, "AssmComCode": modweightmst.AssmComCode, "Weightage": modweightmst.Weightage,
        "NRC": modweightmst.NRC, "Sequence": modweightmst.Sequence
      });
    }

    for (let modweightca of this.ca) {
    temp.push({ "GroupSize": this.GroupSize[modweightca.AssmComCode], "StartWk": this.StartWk[modweightca.AssmComCode], "EndWk": this.EndWk[modweightca.AssmComCode], "Remarks": this.Remarks[modweightca.AssmComCode],
    "Module": modweightca.Module, "AssmComCode": modweightca.AssmComCode, "Weightage": modweightca.Weightage,
    "NRC": modweightca.NRC, "Sequence": modweightca.Sequence })
    }
    this.moduleService.updateWeightage(this.selectedMod.ModuleCode, temp).subscribe();
    alert("Updated Module Weightage Successful!");
  }

  onUpdate() {
    console.log(this.newUpdateReq.value.Requirement);
    this.moduleService.updateModReq(this.selectedMod.MC, this.selectedMod.ModuleCode, this.tempReq.TeachingType, this.newUpdateReq.value.Requirement).subscribe();
    this.moduleService.modReqUpdated.subscribe(() => {
      this.moduleService.loadModReq(this.selectedMod.ModuleCode, this.mc.StaffNo).subscribe((result) => {
        this.data = new MatTableDataSource<ModReq>(result)
      });
    });
    alert("Update Successful");
  }

  updateReq(info) {    
    this.tempReq = info
    this.newUpdateReq = new FormGroup({
      'Type': new FormControl(this.tempReq.TeachingType, [Validators.required]),
      'Requirement': new FormControl(this.tempReq.Requirements, [Validators.required])
    });
  }

}
