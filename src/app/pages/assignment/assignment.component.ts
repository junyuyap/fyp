import { Component, OnInit, ViewChild } from '@angular/core';
import { TasService, AcadYearSem, Course, ModCourse } from 'src/app/services/tas.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';


export interface AcadDisplay {
  Year: string,
  Sem: number,
  Name: string
  YearSem: object;
}

export interface Module {
  ModuleCode: string;
  Type: number;
  ModuleAbbrv: string;
  ModuleName: string;
  MassLecture: string;
  OddWeekLectHr: number;
  EvenWeekLectHr: number;
  OddWeekTutHr: number;
  EvenWeekTutHr: number;
  OddWeekPractHr: number;
  EvenWeekPractHr: number;
  MC: string;
  Moderator: string
}

export interface ForecastAssignment {
  TotalLectClass: number,
  TotalTutClass: number,
  TotalPractClass: number,
  StuStageQty: number,
  Module: string,
  Course: string,
  Year: string,
  Sem: number
}

export interface AcadYearSemDisplay {
  Year: string,
  Sem: string
}


export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

export interface Staff {
  StaffNo: string;
  StaffName: string;
  Section: string;
}

export interface AssignmentDisplay {
  StaffNo: string;
  // SectionID: number;
  TotalHours: number;
  NoLectClass: number;
  NoTutCLass: number;
  NoPractClass: number;
  Module: string;
  EvenWeekLectHr: number;
  EvenWeekTutHr: number;
  EvenWeekPractHr: number;
  // TotalPract: number;
  // TotalLect: number;
  // TotalTut: number;
}

export interface ModuleCourseList {
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string
}

export interface StaffSectionDisplay {
  StaffName: string,
}

export interface ModCourseDisplay {
  StaffName: string,
  Staff: string,
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string,
  ModCourseName: string,
  ModuleAbbrv: string,
  NoLect: number,
  NoPract: number,
  NoTut: number,
  TotalHr: number
}

export interface FullStaffAssignment {
  StaffNo: string,
  StaffName: string,
  Assignment: Array<object>
}

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface ForecastAssignment {
  TotalLectClass: number,
  TotalTutClass: number,
  TotalPractClass: number,
  StuStageQty: number,
  Module: string,
  Course: string,
  Year: string,
  Sem: number
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

export interface Staff {
  StaffNo: string;
  StaffName: string;
  Section: string;
}

export interface sectionStaffList {
  StaffNo: string;
  StaffName: string;
  SectionID: number;
}

export interface ModuleCourseList {
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string
}

export interface ModCourseDisplay {
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string,
  ModCourseName: string,
  ModuleAbbrv: string,
  ModKey: object
}

export interface Module {
  ModuleCode: string;
  Type: number;
  ModuleAbbrv: string;
  ModuleName: string;
  MassLecture: string;
  OddWeekLectHr: number;
  EvenWeekLectHr: number;
  OddWeekTutHr: number;
  EvenWeekTutHr: number;
  OddWeekPractHr: number;
  EvenWeekPractHr: number;
  MC: string;
  Moderator: string;
}

export interface Assignment {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  Staff: string,
}

export interface AssignmentList {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  StaffName: string,
  Staff: string,
  Course: string,
  Module: string
}

export interface ModuleHours {
  LectHr: number,
  PractHr: number,
  TutHr: number
}

export interface ForecastTotal {
  TotalLect: number,
  TotalPract: number,
  TotalTut: number,
  StuStageQty: number
}

export interface Edit {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  Staff: string,
  StaffName: string
}

export interface TotalAssigned {
  TotalLect: number,
  TotalPract: number,
  TotalTut: number
}

export interface DeleteInfo {
  Module: string,
  StaffNo: string,
  Course: string
}

export interface ModCourseDisplaySI {
  StaffName: string,
  Staff: string,
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string,
  ModCourseName: string,
  ModuleAbbrv: string,
  NoLect: number,
  NoPract: number,
  NoTut: number,
  TotalHr: number
}

export interface ModCourseDisplayTAS {
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string,
  ModCourseName: string,
  ModuleAbbrv: string,
  ModKey: object
}

export interface Assignment2 {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  Staff: string,
  Module: string,
  Course: string,
}

export interface acadys{
  Year: string,
  Sem: number
}

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AssignmentComponent implements OnInit {

  
  sectionInfo: Section[] = [];

  staffInfo: Info[] = [];

  staffSectionList: sectionStaffList[] = [];

  display: Assignment[] = [];

  display2: Staff[] = [];

  selected: number;

  modCourseDisplay: ModCourseDisplayTAS[] = [];

  modCourseDisplay2: ModCourseDisplaySI;

  modCourse: ModuleCourseList[] = [];

  modCourseInfo: Module[] = [];

  moduleList: Module[] = [];

  assignmentList: Assignment[] = [];

  assignmentDisplay: AssignmentList[] = [];

  assignmentDisplay2: AssignmentDisplay[] = [];

  newAssignment: FormGroup;

  tempAssignment: Assignment2;

  displayedColumns: string[] = ['StaffName', 'NoLectClass', 'NoPractClass', 'NoTutClass', 'action'];
  data = new MatTableDataSource(this.assignmentDisplay);

  // NEew Assignment Values

  selectedMC: string;
  selectedModuleCourse;

  moduleHours: ModuleHours;

  forecastOn: boolean;

  moduleKey: object;

  AcadYearSem: acadys;

  forecastList: ForecastAssignment

  forecastTotal: ForecastTotal;

  newMC;

  editStaff: Edit;

  updateAsgnmt: FormGroup;

  totalAssigned: TotalAssigned;

  deleteInfo: DeleteInfo;

  acadData: AcadYearSem[] = [];

  acadDisplay: AcadDisplay[] = [];

  constructor(private tasService: TasService) { }


  NFC: FormGroup;

  tempForecast: ForecastAssignment;

  newModule: FormGroup;

  tempModule: Module;

  courseList: Course[] = [];

  newModCourse: FormGroup;

  tempModCourse: ModCourse;

  dataSource = new MatTableDataSource<sectionStaffList>();
  columnsToDisplay = ['StaffName', 'TotalHours'];
  expandedElement: sectionStaffList | null;

  // Take all staff

  assignmentStaffList: Assignment2[];


  tempassignment: Assignment2[] = [];

  tempAssignment2: Assignment;

  totalHr: number;

  staffSectionDisplay: StaffSectionDisplay;

  modCourseList: ModCourse[] = [];

  fullStaffAssignment = [];

  tempAssignmentArray: ModCourseDisplaySI[] = [];

  glodbalIndex = 0;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  ngOnInit() {
    // Selected Section
    this.AcadYearSem = {
      Year: "1",
      Sem: 2
    }

    this.tasService.currentSelected.subscribe(selected => this.selected = selected)
    this.tasService.currentYearSem.subscribe(info => {this.AcadYearSem = info
      console.log("THISACADYEARSEM",this.AcadYearSem);})
    

    this.tasService.loadAcadYearSem().subscribe((result) => {
      this.acadData = result;
      this.tasService.loadModule().subscribe((result) => {
        this.moduleList = result; 
        this.tasService.loadAcadYearSem().subscribe((result) => {
          this.acadData = result
          this.defaultYearSem()
          this.tasService.loadCourse().subscribe((result) => {
            this.courseList = result;
          });
        });
      });
    });

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;


    this.editStaff = {
      NoLectClass: 1,
      NoPractClass: 2,
      NoTutClass: 3,
      Staff: "s12345",
      StaffName: "ABC"
    }

    this.deleteInfo = {
      Module: 'string',
      StaffNo: 'string',
      Course: 'string'
    }

    this.newAssignment = new FormGroup({
      'NAStaff': new FormControl(null, [Validators.required]),
      'NALecture': new FormControl(null, [Validators.required]),
      'NATutorial': new FormControl(null, [Validators.required]),
      'NAPractical': new FormControl(null, [Validators.required])
    })

    this.updateAsgnmt = new FormGroup({
      'EditLect': new FormControl(null, [Validators.required]),
      'EditPract': new FormControl(null, [Validators.required]),
      'EditTut': new FormControl(null, [Validators.required])
    })

    // Get selected Section from Section.component
    this.tasService.currentSelected.subscribe(selected => {
      this.selected = selected;
      this.filterbySection();
    });

    // Get selectedModKey
    this.tasService.currentModKey.subscribe(modkey => {
      this.moduleKey = modkey;
    })

    this.moduleHours = {
      LectHr: 0,
      PractHr: 0,
      TutHr: 0
    }

    this.forecastTotal = {
      TotalLect: 0,
      TotalPract: 0,
      TotalTut: 0,
      StuStageQty: 0,
    }

    this.totalAssigned = {
      TotalLect: 0,
      TotalPract: 0,
      TotalTut: 0
    }



    // Get Section from DB
    this.getData()
    this.forecastOn = false;

    this.tasService.currentYearSem.subscribe(info => this.AcadYearSem = info)

    this.tasService.assignmentListUpdate.subscribe(() => {

    });


    // Default Value
    // this.tasService.changeSelected(1);
  }

  getData() {
    this.tasService.loadSection().subscribe((result) => {
      this.sectionInfo = result
      // console.log(this.sectionInfo);
    });

    // Get Staff from DB
    this.tasService.loadStaff().subscribe((result) => {
      // console.log(result);
      this.staffInfo = result;
      this.tasService.loadModule().subscribe((result) => {
        this.moduleList = result;
      })
      this.tasService.loadModCourse().subscribe((result) => {
        // console.log(result);
        this.modCourse = result;
        // run functions after get data is complete
        this.getValueWithPromise();
      });
    });

    this.getValueWithPromise()
    
    this.tasService.currentYearSem.subscribe(info => {
      console.log("serviceAcadYear",info)
      this.AcadYearSem = info
    })
    

    this.NFC = new FormGroup({
      'NFCModule': new FormControl(null, [Validators.required]),
      'NFCCourse': new FormControl(null, [Validators.required]),
      'NFCYear': new FormControl(null, [Validators.required]),
      'NFCSem': new FormControl(null, [Validators.required]),
      'NFCLecture': new FormControl(null, [Validators.required]),
      'NFCTutorial': new FormControl(null, [Validators.required]),
      'NFCPractical': new FormControl(null, [Validators.required]),
      'NFCStudent': new FormControl(null, [Validators.required])
    })

    this.newModule = new FormGroup({
      'NMCode': new FormControl(null, [Validators.required]),
      'NMAbbrv': new FormControl(null, [Validators.required]),
      'NMName': new FormControl(null, [Validators.required]),
      'NMML': new FormControl(null, [Validators.required]),
      'NMTYPE': new FormControl(null, [Validators.required]),
      'NMEWL': new FormControl(null, [Validators.required]),
      'NMOWL': new FormControl(null, [Validators.required]),
      'NMEWP': new FormControl(null, [Validators.required]),
      'NMOWP': new FormControl(null, [Validators.required]),
      'NMEWT': new FormControl(null, [Validators.required]),
      'NMOWT': new FormControl(null, [Validators.required])
    })

    this.newModCourse = new FormGroup({
      'NMCModule': new FormControl(null, [Validators.required]),
      'NMCCourse': new FormControl(null, [Validators.required]),
      'NMCYear': new FormControl(null, [Validators.required]),
      'NMCStage': new FormControl(null, [Validators.required]),
      'NMCCat': new FormControl(null, [Validators.required])
    })

    this.newAssignment = new FormGroup({
      'NAStaff': new FormControl(null, [Validators.required]),
      'NALecture': new FormControl(null, [Validators.required]),
      'NATutorial': new FormControl(null, [Validators.required]),
      'NAPractical': new FormControl(null, [Validators.required])
    })

    this.updateAsgnmt = new FormGroup({
      'EditLect': new FormControl(null, [Validators.required]),
      'EditPract': new FormControl(null, [Validators.required]),
      'EditTut': new FormControl(null, [Validators.required])
    })

     // Get selected Section from Section.component
     this.tasService.currentSelected.subscribe(selected => {
      this.selected = selected;
      this.filterbySection();
    });

    // Get selectedModKey
    this.tasService.currentModKey.subscribe(modkey => {
      this.moduleKey = modkey;
    })

    this.moduleHours = {
      LectHr: 0,
      PractHr: 0,
      TutHr: 0
    }

    this.forecastTotal = {
      TotalLect: 0,
      TotalPract: 0,
      TotalTut: 0,
      StuStageQty: 0,
    }

    this.totalAssigned = {
      TotalLect: 0,
      TotalPract: 0,
      TotalTut: 0
    }


  }

  

  concatYearSem(info) {
        this.AcadYearSem = {
          Year: info.Year.substring(0,4) + info.Year.substring(5),
          Sem: info.Sem
        }
      this.tasService.changeYearSem(this.AcadYearSem);
  }

  defaultYearSem() {
    this.tasService.changeYearSem(this.acadData[this.acadData.length - 1])
    this.concatYearSem(this.AcadYearSem)
  }

  selectYearSem(info) {
    // console.log(info)
    this.tasService.changeYearSem(info) 
  }


  addForecast() {
    this.tempForecast = {
      TotalLectClass: this.NFC.value.NFCLecture,
      TotalTutClass: this.NFC.value.NFCTutorial,
      TotalPractClass: this.NFC.value.NFCPractical,
      StuStageQty: this.NFC.value.NFCStudent,
      Module: this.NFC.value.NFCModule,
      Course: this.NFC.value.NFCCourse,
      // @ts-ignore
      Year: this.AcadYearSem.Year,
      // @ts-ignore
      Sem: this.AcadYearSem.Sem
    }
    // console.log(this.tempForecast);
    this.tasService.addForecastAssignment(this.tempForecast, this.AcadYearSem);
  }
  

  addModule() {
    if (this.newModule.valid) {
      this.tempModule = {
        ModuleCode: this.newModule.value.NMCode,
        Type: this.newModule.value.NMType,
        ModuleAbbrv: this.newModule.value.NMAbbrv,
        ModuleName: this.newModule.value.NMName,
        MassLecture: this.newModule.value.NMML,
        OddWeekLectHr: this.newModule.value.NMOWL,
        EvenWeekLectHr: this.newModule.value.NMEWL,
        OddWeekTutHr: this.newModule.value.NMOWT,
        EvenWeekTutHr: this.newModule.value.NMEWT,
        OddWeekPractHr: this.newModule.value.NMOWP,
        EvenWeekPractHr: this.newModule.value.NMEWP,
        MC: null,
        Moderator: null
      }
      this.tasService.addModule(this.tempModule);
    }
    else {
      alert("one or more values missing or invalid")
    }
    // console.log(this.tempModule);
    
  }

  addModCourse() {
    if (this.newModCourse.valid) {
      this.tempModCourse = {
        ModCat: this.newModCourse.value.NMCCat,
        CourseYear: this.newModCourse.value.NMCYear,
        Stage: this.newModCourse.value.NMCStage,
        Module: this.newModCourse.value.NMCModule,
        Course: this.newModCourse.value.NMCCourse
      }
    }
    this.tasService.addModuleCourse(this.tempModCourse);
    window.location.reload();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  filterSection() {
    this.staffSectionList = [];
    for (let staff of this.staffInfo) {
      for (let section of this.sectionInfo) {
        if (staff.Section === this.selected && staff.Section === section.SectionID) {
          this.display2.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
          this.staffSectionList.push({
            StaffNo: staff.StaffNo,
            StaffName: staff.StaffName,
            SectionID: staff.Section,
          })
        }
      }
    }
    this.resolveAfter2Seconds(20).then(value => {
      this.fullStaffAssignment = [];
      this.getStaffAssignment()
    });


    // console.log("FILTER SECTION RAN");
    // console.log(this.staffSectionList);
    //  Set selected value
    this.tasService.changeSelected(this.selected);
    // console.log(this.selected);
  }

  getModuleSection() {
    for (let assignment of this.tempassignment) {
      for (let module of this.moduleList) {
        if (assignment.Module === module.ModuleCode) {
          this.totalHr = (assignment.NoLectClass * module.EvenWeekLectHr) + (assignment.NoTutClass * module.EvenWeekTutHr) + (assignment.NoPractClass * module.EvenWeekPractHr)
          this.assignmentDisplay2.push({
            StaffNo: assignment.Staff,
            TotalHours: this.totalHr,
            NoLectClass: assignment.NoLectClass,
            NoTutCLass: assignment.NoLectClass,
            NoPractClass: assignment.NoLectClass,
            Module: module.ModuleCode,
            EvenWeekLectHr: module.EvenWeekLectHr,
            EvenWeekTutHr: module.EvenWeekTutHr,
            EvenWeekPractHr: module.EvenWeekPractHr
          })
        }
      }
    }
    // console.log("asgndisplay", this.assignmentDisplay)
  }

  getStaffAssignment() {
    for (let staff of this.staffSectionList) {
      // console.log("staff", staff)
      this.tasService.loadAssignmentByStaff(staff.StaffNo, this.AcadYearSem).subscribe((result) => {
        this.assignmentStaffList = result;
        // console.log("get staff assignments", result);
        for (let mc of this.assignmentStaffList) {
          this.tasService.getSpecificModCourse(mc.Course, mc.Module, this.AcadYearSem).subscribe((result) => {
            // console.log("specificModCourse", result)
            var L = mc.NoLectClass
            var T = mc.NoTutClass
            var P = mc.NoPractClass
            var SNo = mc.Staff
            var SName = staff.StaffName
            this.modCourseConcat(result, this.moduleList, L, T, P, SNo, SName)
          });
        }
      });
    }
  }

  modCourseConcat(result, modList, L, T, P, SNo, SName) {
    for (let module of modList) {
      if (result.Module === module.ModuleCode) {
        const string = result.Module + " " + module.ModuleAbbrv + ": (" + result.Course + ") Yr " + result.CourseYear + result.Stage
        var totalHr = (L * module.EvenWeekLectHr) + (T * module.EvenWeekTutHr) + (P * module.EvenWeekPractHr)
        this.modCourseDisplay2 = {
          StaffName: SName,
          Staff: SNo,
          ModCat: result.ModCat,
          CourseYear: result.CourseYear,
          Stage: result.Stage,
          Module: result.Module,
          Course: result.Course,
          ModCourseName: string,
          ModuleAbbrv: module.ModuleAbbrv,
          NoLect: L,
          NoPract: P,
          NoTut: T,
          TotalHr: totalHr
        }
        this.tempAssignmentArray.push(this.modCourseDisplay2)
        console.log("tempassignemntarr",this.tempAssignmentArray)

        // if (this.fullStaffAssignment[SNo] == null) {
        //   this.fullStaffAssignment[SNo] = [];
        // };


        const existing = this.fullStaffAssignment.map(x => x['staff']);
        if (existing.indexOf(SNo) != -1) {
          this.fullStaffAssignment[existing.indexOf(SNo)]['assignments'].push(this.modCourseDisplay2);
          this.fullStaffAssignment[this.fullStaffAssignment.map(x => x['staff']).indexOf(SNo)]['TotalHr'] += totalHr;
        } else {
          this.fullStaffAssignment.push({ staff: SNo, staffName: SName, TotalHr: totalHr, assignments: [] });
          this.fullStaffAssignment[this.fullStaffAssignment.length - 1]['assignments'].push(this.modCourseDisplay2);
        };
        // console.log("TEMP ASSIGNMENT ARRAY", this.tempAssignmentArray)
      }
    }
    // console.log("modcourseDisplay", this.modCourseDisplay);
    // console.log('testing', this.fullStaffAssignment);
    this.dataSource = new MatTableDataSource(this.fullStaffAssignment); // this.staffsectionList after filter section. 
    console.log('testing2', this.dataSource)
    this.dataSource.paginator = this.paginator
  }

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 1000);
    });
  }

  // load forecast by selectedModCourse

  // Add assignment works. CURRENT ISSUES: TABLE DOESNT UPDATE REALTIME
  addAssignment() {
    // console.log("Adding Assignment", this.newAssignment)
    if (this.newAssignment.valid) {
      this.tempAssignment2 = {
        "NoLectClass": this.newAssignment.value.NALecture,
        "NoTutClass": this.newAssignment.value.NATutorial,
        "NoPractClass": this.newAssignment.value.NAPractical,
        "Staff": this.newAssignment.value.NAStaff
      }
      console.log('tempassign', this.tempAssignment2)
      this.tasService.addAssignment(this.selectedModuleCourse.Course, this.selectedModuleCourse.ModuleID, this.tempAssignment2, this.AcadYearSem);
      
      // Reload Assignment List
      this.resolveAfter2Seconds(20).then(value => {
        this.tasService.loadAssignmentByModule(this.selectedModuleCourse, this.AcadYearSem).subscribe((result) => {
          this.assignmentList = [];
          this.assignmentDisplay = [];
          // console.log(result);
          this.assignmentList = result;
          this.loadAssignmentList(this.assignmentDisplay);
          this.filterSection()
        });

      });
    }
    else {
      alert("One or more of the fields is empty!")
    }
  }

  goingToDeleteAssignment(Module, Course, StaffNo) {
    this.deleteInfo = {
      Module: Module,
      Course: Course,
      StaffNo: StaffNo
    }
    // console.log("deleteinfo", this.deleteInfo)
  }

  confirmDelete() {
    var Module = this.deleteInfo.Module
    var Course = this.deleteInfo.Course
    var StaffNo = this.deleteInfo.StaffNo
    this.tasService.removeAssignment(Module, Course, StaffNo, this.AcadYearSem);
    // console.log("GOING TO DELETE", Course, Module, StaffNo, this.AcadYearSem);
    // Reload Assignment List
    this.resolveAfter2Seconds(20).then(value => {
      this.tasService.loadAssignmentByModule(this.selectedModuleCourse, this.AcadYearSem).subscribe((result) => {
        this.assignmentList = [];
        this.assignmentDisplay = [];
        // console.log(result);
        this.assignmentList = result;
        this.loadAssignmentList(this.assignmentDisplay);
        this.filterSection()
      });

    });

  }


  loadSelectedModCourse(object) {
    if (JSON.stringify(this.AcadYearSem) === '{}') {
      alert("Please select a Dataset first")
    }
    else {
      this.forecastOn = true;
      this.selectedModuleCourse = object;
      this.tasService.changeModKey(this.selectedModuleCourse)
      this.loadCurrentFC(this.selectedModuleCourse)
      // console.log("selectedmodcourseInfo", this.modCourseInfo);
      // console.log('load selectedmodcourse', this.selectedModuleCourse);
      this.assignmentList = [];
      this.assignmentDisplay = [];
      this.tasService.loadAssignmentByModule(object, this.AcadYearSem).subscribe((result) => {
        // console.log(result);
        this.assignmentList = result;
        this.loadAssignmentList(object);
        if (this.assignmentDisplay.length < 1) {
          this.assignmentDisplay = [{
            NoLectClass: 0,
            NoTutClass: 0,
            NoPractClass: 0,
            StaffName: "No Current Assignments",
            Staff: "blank",
            Course: "blank",
            Module: "blank"
          }]
          this.data = new MatTableDataSource(this.assignmentDisplay);
        }
        this.tasService.loadModule().subscribe((result) => {
          // console.log("modList", result);
          this.moduleList = result;
          for (let elem of this.moduleList) {
            if (elem.ModuleCode === object.ModuleID) {
              // console.log("MC", elem)
              // console.log("MC", elem.MC)
              this.selectedMC = elem.MC;
            }
          }
        });
        });
    }
  }

  loadCurrentFC(object) {
    // console.log("forecast obj", object)
    // console.log(this.AcadYearSem)
    this.tasService.loadForecastAssignment(object.Course, object.ModuleID, this.AcadYearSem).subscribe((result) => {
      this.forecastList = result;
      this.forecastTotal = {
        TotalLect: result.TotalLectClass,
        TotalPract: result.TotalPractClass,
        TotalTut: result.TotalTutClass,
        StuStageQty: result.StuStageQty
      }
      // console.log("forecast", result)
    });
  }

  loadAssignmentList(object) {
    var totalL = 0;
    var totalP = 0;
    var totalT = 0;
    for (let elem of this.assignmentList) {
      for (let staff of this.staffInfo) {
        if (elem.Staff === staff.StaffNo) {
          this.assignmentDisplay.push({
            NoLectClass: elem.NoLectClass,
            NoTutClass: elem.NoTutClass,
            NoPractClass: elem.NoPractClass,
            StaffName: staff.StaffName,
            Staff: elem.Staff,
            Course: this.selectedModuleCourse.Course,
            Module: this.selectedModuleCourse.ModuleID
          })
          totalL = totalL + elem.NoLectClass;
          totalP = totalP + elem.NoPractClass;
          totalT = totalT + elem.NoTutClass
          this.totalAssigned = {
            TotalLect: totalL,
            TotalPract: totalP,
            TotalTut: totalT
          }
        }
      }
    }
    // console.log("Display", this.assignmentDisplay);
    this.data = new MatTableDataSource(this.assignmentDisplay);
  }


  filterbySection() {
    this.staffSectionList = [];
    this.display = [];
    for (let staff of this.staffInfo) {
      for (let section of this.sectionInfo) {
        if (staff.Section === this.selected && staff.Section === section.SectionID) {
          // this.display.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
          this.staffSectionList.push({
            StaffNo: staff.StaffNo,
            StaffName: staff.StaffName,
            SectionID: staff.Section,
          })
        }
      }
    }
    // this.data = new MatTableDataSource(this.display)
    // console.log("filterbysection")
    // console.log(this.display);
    // console.log(this.staffSectionList);
  }

  // Wait after promise is resolved then callback
  getValueWithPromise() {
    this.resolveAfter2Seconds(20).then(value => {
      this.filterbySection();
      this.modCourseConcat2(this.modCourse, this.moduleList)

    });
  }

  modCourseConcat2(result, modList) {
    for (let modCourse of result) {
      for (let module of modList) {
        if (modCourse.Module === module.ModuleCode) {
          const string = modCourse.Module + " " + module.ModuleAbbrv + ": (" + modCourse.Course + ") Yr " + modCourse.CourseYear + modCourse.Stage
          this.modCourseDisplay.push({
            ModCat: modCourse.ModCat,
            CourseYear: modCourse.CourseYear,
            Stage: modCourse.Stage,
            Module: modCourse.Module,
            Course: modCourse.Course,
            ModCourseName: string,
            ModuleAbbrv: module.ModuleAbbrv,
            ModKey: {
              ModuleID: modCourse.Module,
              CourseYear: modCourse.CourseYear,
              Course: modCourse.Course,
            }

          })
          this.moduleHours = {
            LectHr: module.EvenWeekLectHr,
            PractHr: module.EvenWeekPractHr,
            TutHr: module.EvenWeekTutHr,
          }
        }
      }
    }
    // console.log(this.modCourseDisplay)
  }

  selectMC(data) {
    this.newMC = data
  }


  saveMC() {
    const module = this.selectedModuleCourse.ModuleID;
    // console.log("selectedmodule", module)
    this.tasService.updateMC(module, this.newMC).subscribe()
  }

  updateAssignment(info) {
    const module = this.selectedModuleCourse.ModuleID;
    const course = this.selectedModuleCourse.Course
    // console.log(info)
    this.editStaff = {
      NoLectClass: info.NoLectClass,
      NoPractClass: info.NoPractClass,
      NoTutClass: info.NoTutClass,
      Staff: info.Staff,
      StaffName: info.StaffName
    }

  }

  confirmUpdate() {
    if (this.updateAsgnmt.valid) {
      this.editStaff = {
        NoLectClass: this.updateAsgnmt.value.EditLect,
        NoPractClass: this.updateAsgnmt.value.EditPract,
        NoTutClass: this.updateAsgnmt.value.EditTut,
        Staff: this.editStaff.Staff,
        StaffName: this.editStaff.StaffName
      }
      console.log("edit", this.editStaff)
      const module = this.selectedModuleCourse.ModuleID;
      const course = this.selectedModuleCourse.Course
      // console.log(this.editStaff, course, module, this.AcadYearSem)
      this.tasService.updateAssignment(this.editStaff, course, module, this.AcadYearSem).subscribe();
      // Reload Assignment List
      this.resolveAfter2Seconds(20).then(value => {
        this.tasService.loadAssignmentByModule(this.selectedModuleCourse, this.AcadYearSem).subscribe((result) => {
          this.assignmentList = [];
          this.assignmentDisplay = [];
          // console.log(result);
          this.assignmentList = result;
          this.loadAssignmentList(this.assignmentDisplay);
          this.filterSection()
        });

      });

    }
  }




}



// Add new forecast [X], New ModCourse [X] (Maybe Reload Modcourse Data), update Assignment [X], Table update realtime [X] , Calculate Assigned[X], shitton of double checking-error & confirmation [ ], section info [ ]