import { Component, OnInit, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { TasService } from 'src/app/services/tas.service';

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface ForecastAssignment {
  TotalLectClass: number,
  TotalTutClass: number,
  TotalPractClass: number,
  StuStageQty: number,
  Module: string,
  Course: string,
  Year: string,
  Sem: number
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

export interface Staff {
  StaffNo: string;
  StaffName: string;
  Section: string;
}

export interface sectionStaffList {
  StaffNo: string;
  StaffName: string;
  SectionID: number;
}

export interface ModuleCourseList {
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string
}

export interface ModCourseDisplay {
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string,
  ModCourseName: string,
  ModuleAbbrv: string,
  ModKey: object
}

export interface Module {
  ModuleCode: string;
  Type: number;
  ModuleAbbrv: string;
  ModuleName: string;
  MassLecture: string;
  OddWeekLectHr: number;
  EvenWeekLectHr: number;
  OddWeekTutHr: number;
  EvenWeekTutHr: number;
  OddWeekPractHr: number;
  EvenWeekPractHr: number;
  MC: string;
  Moderator: string;
}

export interface Assignment {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  Staff: string
}

export interface AssignmentList {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  StaffName: string,
  Staff: string,
  Course: string,
  Module: string
}

export interface ModuleHours {
  LectHr: number,
  PractHr: number,
  TutHr: number
}

export interface ForecastTotal {
  TotalLect: number,
  TotalPract: number,
  TotalTut: number,
  StuStageQty: number
}

export interface Edit {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  Staff: string,
  StaffName: string
}

export interface TotalAssigned {
  TotalLect: number,
  TotalPract: number,
  TotalTut: number
}

export interface DeleteInfo {
  Module: string,
  StaffNo: string,
  Course: string
}

@Component({
  selector: 'app-tas',
  templateUrl: './tas.component.html',
  styleUrls: ['./tas.component.css']
})
export class TasComponent implements OnInit {

  constructor(private tasService: TasService) { }

  sectionInfo: Section[] = [];

  staffInfo: Info[] = [];

  staffSectionList: sectionStaffList[] = [];

  display: Assignment[] = [];

  selected: number;

  modCourseDisplay: ModCourseDisplay[] = [];

  modCourse: ModuleCourseList[] = [];

  modCourseInfo: Module[] = [];

  moduleList: Module[] = [];

  assignmentList: Assignment[] = [];

  assignmentDisplay: AssignmentList[] = [];

  newAssignment: FormGroup;

  tempAssignment: Assignment;

  displayedColumns: string[] = ['StaffName', 'NoLectClass', 'NoPractClass', 'NoTutClass', 'action'];
  data = new MatTableDataSource(this.assignmentDisplay);

  // NEew Assignment Values

  selectedMC: string;
  selectedModuleCourse;

  moduleHours: ModuleHours;

  forecastOn: boolean;

  moduleKey: object;

  AcadYearSem: object;

  forecastList: ForecastAssignment

  forecastTotal: ForecastTotal;

  newMC;

  editStaff: Edit;

  updateAsgnmt: FormGroup;

  totalAssigned: TotalAssigned;

  deleteInfo: DeleteInfo;

  sectionlistupdate = new EventEmitter<void>();

  ngOnInit(): void {

    this.editStaff = {
      NoLectClass: 1,
      NoPractClass: 2,
      NoTutClass: 3,
      Staff: "s12345",
      StaffName: "ABC"
    }

    this.deleteInfo = {
      Module: 'string',
      StaffNo: 'string',
      Course: 'string'
    }

    this.newAssignment = new FormGroup({
      'NAStaff': new FormControl(null, [Validators.required]),
      'NALecture': new FormControl(null, [Validators.required]),
      'NATutorial': new FormControl(null, [Validators.required]),
      'NAPractical': new FormControl(null, [Validators.required])
    })

    this.updateAsgnmt = new FormGroup({
      'EditLect': new FormControl(null, [Validators.required]),
      'EditPract': new FormControl(null, [Validators.required]),
      'EditTut': new FormControl(null, [Validators.required])
    })

    // Get selected Section from Section.component
    this.tasService.currentSelected.subscribe(selected => {
      this.selected = selected;
      this.filterbySection();
    });

    // Get selectedModKey
    this.tasService.currentModKey.subscribe(modkey => {
      this.moduleKey = modkey;
    })

    this.moduleHours = {
      LectHr: 0,
      PractHr: 0,
      TutHr: 0
    }

    this.forecastTotal = {
      TotalLect: 0,
      TotalPract: 0,
      TotalTut: 0,
      StuStageQty: 0,
    }

    this.totalAssigned = {
      TotalLect: 0,
      TotalPract: 0,
      TotalTut: 0
    }



    // Get Section from DB
    this.getData()
    this.forecastOn = false;

    this.tasService.currentYearSem.subscribe(info => this.AcadYearSem = info)

    this.tasService.assignmentListUpdate.subscribe(() => {

    });
  }

  getData() {
    this.tasService.loadSection().subscribe((result) => {
      this.sectionInfo = result
      // console.log(this.sectionInfo);
      this.tasService.loadStaff().subscribe((result) => {
        // console.log(result);
        this.staffInfo = result;
        this.tasService.loadModule().subscribe((result) => {
          // console.log("modList", result);
          this.moduleList = result;
          this.tasService.loadModCourse().subscribe((result) => {
            // console.log(result);
            this.modCourse = result;
            // run functions after get data is complete
            this.getValueWithPromise();
          });
        });
      });
    });
  }

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 1000);
    });
  }

  // load forecast by selectedModCourse

  // Add assignment works. CURRENT ISSUES: TABLE DOESNT UPDATE REALTIME
  addAssignment() {
    // console.log("Adding Assignment", this.newAssignment)
    if (this.newAssignment.valid) {
      this.tempAssignment = {
        "NoLectClass": this.newAssignment.value.NALecture,
        "NoTutClass": this.newAssignment.value.NATutorial,
        "NoPractClass": this.newAssignment.value.NAPractical,
        "Staff": this.newAssignment.value.NAStaff
      }
      // console.log('tempassign', this.tempAssignment)
      this.tasService.addAssignment(this.selectedModuleCourse.Course, this.selectedModuleCourse.ModuleID, this.tempAssignment, this.AcadYearSem);
      
      // Reload Assignment List
      this.resolveAfter2Seconds(20).then(value => {
        this.tasService.loadAssignmentByModule(this.selectedModuleCourse, this.AcadYearSem).subscribe((result) => {
          this.assignmentList = [];
          this.assignmentDisplay = [];
          // console.log(result);
          this.assignmentList = result;
          this.loadAssignmentList(this.assignmentDisplay);
          this.sectionlistupdate.emit()
        });

      });
    }
    else {
      alert("One or more of the fields is empty!")
    }
  }

  goingToDeleteAssignment(Module, Course, StaffNo) {
    this.deleteInfo = {
      Module: Module,
      Course: Course,
      StaffNo: StaffNo
    }
    // console.log("deleteinfo", this.deleteInfo)
  }

  confirmDelete() {
    var Module = this.deleteInfo.Module
    var Course = this.deleteInfo.Course
    var StaffNo = this.deleteInfo.StaffNo
    this.tasService.removeAssignment(Module, Course, StaffNo, this.AcadYearSem);
    // console.log("GOING TO DELETE", Course, Module, StaffNo, this.AcadYearSem);
    // Reload Assignment List
    this.resolveAfter2Seconds(20).then(value => {
      this.tasService.loadAssignmentByModule(this.selectedModuleCourse, this.AcadYearSem).subscribe((result) => {
        this.assignmentList = [];
        this.assignmentDisplay = [];
        // console.log(result);
        this.assignmentList = result;
        this.loadAssignmentList(this.assignmentDisplay);
        this.sectionlistupdate.emit()
      });

    });

  }


  loadSelectedModCourse(object) {
    if (JSON.stringify(this.AcadYearSem) === '{}') {
      alert("Please select a Dataset first")
    }
    else {
      this.forecastOn = true;
      this.selectedModuleCourse = object;
      this.tasService.changeModKey(this.selectedModuleCourse)
      this.loadCurrentFC(this.selectedModuleCourse)
      // console.log("selectedmodcourseInfo", this.modCourseInfo);
      // console.log('load selectedmodcourse', this.selectedModuleCourse);
      this.assignmentList = [];
      this.assignmentDisplay = [];
      this.tasService.loadAssignmentByModule(object, this.AcadYearSem).subscribe((result) => {
        // console.log(result);
        this.assignmentList = result;
        this.loadAssignmentList(object);
        if (this.assignmentDisplay.length < 1) {
          this.assignmentDisplay = [{
            NoLectClass: 0,
            NoTutClass: 0,
            NoPractClass: 0,
            StaffName: "No Current Assignments",
            Staff: "blank",
            Course: "blank",
            Module: "blank"
          }]
          this.data = new MatTableDataSource(this.assignmentDisplay);
        }
        this.tasService.loadModule().subscribe((result) => {
          // console.log("modList", result);
          this.moduleList = result;
          for (let elem of this.moduleList) {
            if (elem.ModuleCode === object.ModuleID) {
              // console.log("MC", elem)
              // console.log("MC", elem.MC)
              this.selectedMC = elem.MC;
            }
          }
        });
        });
    }
  }

  loadCurrentFC(object) {
    // console.log("forecast obj", object)
    // console.log(this.AcadYearSem)
    this.tasService.loadForecastAssignment(object.Course, object.ModuleID, this.AcadYearSem).subscribe((result) => {
      this.forecastList = result;
      this.forecastTotal = {
        TotalLect: result.TotalLectClass,
        TotalPract: result.TotalPractClass,
        TotalTut: result.TotalTutClass,
        StuStageQty: result.StuStageQty
      }
      // console.log("forecast", result)
    });
  }

  loadAssignmentList(object) {
    var totalL = 0;
    var totalP = 0;
    var totalT = 0;
    for (let elem of this.assignmentList) {
      for (let staff of this.staffInfo) {
        if (elem.Staff === staff.StaffNo) {
          this.assignmentDisplay.push({
            NoLectClass: elem.NoLectClass,
            NoTutClass: elem.NoTutClass,
            NoPractClass: elem.NoPractClass,
            StaffName: staff.StaffName,
            Staff: elem.Staff,
            Course: this.selectedModuleCourse.Course,
            Module: this.selectedModuleCourse.ModuleID
          })
          totalL = totalL + elem.NoLectClass;
          totalP = totalP + elem.NoPractClass;
          totalT = totalT + elem.NoTutClass
          this.totalAssigned = {
            TotalLect: totalL,
            TotalPract: totalP,
            TotalTut: totalT
          }
        }
      }
    }
    // console.log("Display", this.assignmentDisplay);
    this.data = new MatTableDataSource(this.assignmentDisplay);
  }


  filterbySection() {
    this.staffSectionList = [];
    this.display = [];
    for (let staff of this.staffInfo) {
      for (let section of this.sectionInfo) {
        if (staff.Section === this.selected && staff.Section === section.SectionID) {
          // this.display.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
          this.staffSectionList.push({
            StaffNo: staff.StaffNo,
            StaffName: staff.StaffName,
            SectionID: staff.Section,
          })
        }
      }
    }
    // this.data = new MatTableDataSource(this.display)
    // console.log("filterbysection")
    // console.log(this.display);
    // console.log(this.staffSectionList);
  }

  // Wait after promise is resolved then callback
  getValueWithPromise() {
    this.resolveAfter2Seconds(20).then(value => {
      this.filterbySection();
      this.modCourseConcat(this.modCourse, this.moduleList)

    });
  }

  modCourseConcat(result, modList) {
    for (let modCourse of result) {
      for (let module of modList) {
        if (modCourse.Module === module.ModuleCode) {
          const string = modCourse.Module + " " + module.ModuleAbbrv + ": (" + modCourse.Course + ") Yr " + modCourse.CourseYear + modCourse.Stage
          this.modCourseDisplay.push({
            ModCat: modCourse.ModCat,
            CourseYear: modCourse.CourseYear,
            Stage: modCourse.Stage,
            Module: modCourse.Module,
            Course: modCourse.Course,
            ModCourseName: string,
            ModuleAbbrv: module.ModuleAbbrv,
            ModKey: {
              ModuleID: modCourse.Module,
              CourseYear: modCourse.CourseYear,
              Course: modCourse.Course,
            }

          })
          this.moduleHours = {
            LectHr: module.EvenWeekLectHr,
            PractHr: module.EvenWeekPractHr,
            TutHr: module.EvenWeekTutHr,
          }
        }
      }
    }
    // console.log(this.modCourseDisplay)
  }

  selectMC(data) {
    this.newMC = data
  }


  saveMC() {
    const module = this.selectedModuleCourse.ModuleID;
    // console.log("selectedmodule", module)
    this.tasService.updateMC(module, this.newMC).subscribe()
  }

  updateAssignment(info) {
    const module = this.selectedModuleCourse.ModuleID;
    const course = this.selectedModuleCourse.Course
    // console.log(info)
    this.editStaff = {
      NoLectClass: info.NoLectClass,
      NoPractClass: info.NoPractClass,
      NoTutClass: info.NoTutClass,
      Staff: info.Staff,
      StaffName: info.StaffName
    }

  }

  confirmUpdate() {
    if (this.updateAsgnmt.valid) {
      this.editStaff = {
        NoLectClass: this.updateAsgnmt.value.EditLect,
        NoPractClass: this.updateAsgnmt.value.EditPract,
        NoTutClass: this.updateAsgnmt.value.EditTut,
        Staff: this.editStaff.Staff,
        StaffName: this.editStaff.StaffName
      }
      // console.log("edit", this.editStaff)
      const module = this.selectedModuleCourse.ModuleID;
      const course = this.selectedModuleCourse.Course
      // console.log(this.editStaff, course, module, this.AcadYearSem)
      this.tasService.updateAssignment(this.editStaff, course, module, this.AcadYearSem).subscribe();
      // Reload Assignment List
      this.resolveAfter2Seconds(20).then(value => {
        this.tasService.loadAssignmentByModule(this.selectedModuleCourse, this.AcadYearSem).subscribe((result) => {
          this.assignmentList = [];
          this.assignmentDisplay = [];
          // console.log(result);
          this.assignmentList = result;
          this.loadAssignmentList(this.assignmentDisplay);
          this.sectionlistupdate.emit()
        });

      });

    }
  }

  


}
