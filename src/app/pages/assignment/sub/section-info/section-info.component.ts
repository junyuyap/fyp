import { Component, OnInit, ViewChild, Type } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { TasService, Module, ModCourse } from '../../../../services/tas.service'


export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

export interface Staff {
  StaffNo: string;
  StaffName: string;
  Section: string;
}

export interface sectionStaffList {
  StaffNo: string;
  StaffName: string;
  SectionID: number;
}

export interface Assignment {
  NoLectClass: number,
  NoTutClass: number,
  NoPractClass: number,
  Staff: string,
  Module: string,
  Course: string,
}

export interface AssignmentDisplay {
  StaffNo: string;
  // SectionID: number;
  TotalHours: number;
  NoLectClass: number;
  NoTutCLass: number;
  NoPractClass: number;
  Module: string;
  EvenWeekLectHr: number;
  EvenWeekTutHr: number;
  EvenWeekPractHr: number;
  // TotalPract: number;
  // TotalLect: number;
  // TotalTut: number;
}

export interface ModuleCourseList {
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string
}

export interface StaffSectionDisplay {
  StaffName: string,
}

export interface ModCourseDisplay {
  StaffName: string,
  Staff: string,
  ModCat: string,
  CourseYear: number,
  Stage: string,
  Module: string,
  Course: string,
  ModCourseName: string,
  ModuleAbbrv: string,
  NoLect: number,
  NoPract: number,
  NoTut: number,
  TotalHr: number
}

export interface FullStaffAssignment {
  StaffNo: string,
  StaffName: string,
  Assignment: Array<object>
}

@Component({
  selector: 'app-section-info',
  templateUrl: './section-info.component.html',
  styleUrls: ['./section-info.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class SectionInfoComponent implements OnInit {

  constructor(private tasService: TasService) { }

  selected: number;

  dataSource = new MatTableDataSource<sectionStaffList>();
  columnsToDisplay = ['StaffName', 'TotalHours'];
  expandedElement: sectionStaffList | null;

  // Take all staff
  sectionInfo: Section[] = [];

  staffInfo: Info[] = [];

  staffSectionList: sectionStaffList[] = [
  ];

  assignmentStaffList: Assignment[];

  display: Staff[] = [];

  AcadYearSem: object;

  assignmentDisplay: AssignmentDisplay[] = [];

  tempassignment: Assignment[] = [];

  moduleList: Module[] = [];

  totalHr: number;

  modCourse: ModuleCourseList[] = [];

  staffSectionDisplay: StaffSectionDisplay;

  modCourseDisplay: ModCourseDisplay;

  modCourseList: ModCourse[] = [];

  fullStaffAssignment = [];

  tempAssignmentArray: ModCourseDisplay[] = [];

  glodbalIndex = 0;


  // result = sectionStaffList

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    // Selected Section
    this.tasService.currentSelected.subscribe(selected => this.selected = selected)
    this.tasService.currentYearSem.subscribe(info => this.AcadYearSem = info)

    this.getData()


    // Default Value
    // this.tasService.changeSelected(1);
  }

  getData() {
    this.tasService.loadSection().subscribe((result) => {
      this.sectionInfo = result
      // console.log(this.sectionInfo);
    });

    // Get Staff from DB
    this.tasService.loadStaff().subscribe((result) => {
      // console.log(result);
      this.staffInfo = result;
      this.tasService.loadModule().subscribe((result) => {
        this.moduleList = result;
      })
      this.tasService.loadModCourse().subscribe((result) => {
        // console.log(result);
        this.modCourse = result;
        // run functions after get data is complete
        this.getValueWithPromise();
      });
    });

    this.getValueWithPromise()
  }



  // EVENTUALLY NEEDS TO BE CHANGED TO RESOLVE AFTER GETDATA()
  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 1000);
    });
  }

  // Wait after promise(wait 2 seconds) then 
  getValueWithPromise() {
    this.resolveAfter2Seconds(20).then(value => {

    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  filterSection() {
    this.staffSectionList = [];
    for (let staff of this.staffInfo) {
      for (let section of this.sectionInfo) {
        if (staff.Section === this.selected && staff.Section === section.SectionID) {
          this.display.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
          this.staffSectionList.push({
            StaffNo: staff.StaffNo,
            StaffName: staff.StaffName,
            SectionID: staff.Section,
          })
        }
      }
    }
    this.resolveAfter2Seconds(20).then(value => {
      this.fullStaffAssignment = [];
      this.getStaffAssignment()
    });


    // console.log("FILTER SECTION RAN");
    // console.log(this.staffSectionList);
    //  Set selected value
    this.tasService.changeSelected(this.selected);
    // console.log(this.selected);
  }

  getModuleSection() {
    for (let assignment of this.tempassignment) {
      for (let module of this.moduleList) {
        if (assignment.Module === module.ModuleCode) {
          this.totalHr = (assignment.NoLectClass * module.EvenWeekLectHr) + (assignment.NoTutClass * module.EvenWeekTutHr) + (assignment.NoPractClass * module.EvenWeekPractHr)
          this.assignmentDisplay.push({
            StaffNo: assignment.Staff,
            TotalHours: this.totalHr,
            NoLectClass: assignment.NoLectClass,
            NoTutCLass: assignment.NoLectClass,
            NoPractClass: assignment.NoLectClass,
            Module: module.ModuleCode,
            EvenWeekLectHr: module.EvenWeekLectHr,
            EvenWeekTutHr: module.EvenWeekTutHr,
            EvenWeekPractHr: module.EvenWeekPractHr
          })
        }
      }
    }
    // console.log("asgndisplay", this.assignmentDisplay)
  }

  getStaffAssignment() {
    for (let staff of this.staffSectionList) {
      // console.log("staff", staff)
      this.tasService.loadAssignmentByStaff(staff.StaffNo, this.AcadYearSem).subscribe((result) => {
        this.assignmentStaffList = result;
        // console.log("get staff assignments", result);
        for (let mc of this.assignmentStaffList) {
          this.tasService.getSpecificModCourse(mc.Course, mc.Module, this.AcadYearSem).subscribe((result) => {
            // console.log("specificModCourse", result)
            var L = mc.NoLectClass
            var T = mc.NoTutClass
            var P = mc.NoPractClass
            var SNo = mc.Staff
            var SName = staff.StaffName
            this.modCourseConcat(result, this.moduleList, L, T, P, SNo, SName)
          });
        }
      });
    }
  }

  modCourseConcat(result, modList, L, T, P, SNo, SName) {
    for (let module of modList) {
      if (result.Module === module.ModuleCode) {
        const string = result.Module + " " + module.ModuleAbbrv + ": (" + result.Course + ") Yr " + result.CourseYear + result.Stage
        var totalHr = (L * module.EvenWeekLectHr) + (T * module.EvenWeekTutHr) + (P * module.EvenWeekPractHr)
        this.modCourseDisplay = {
          Staff: SNo,
          StaffName: SName,
          ModCat: result.ModCat,
          CourseYear: result.CourseYear,
          Stage: result.Stage,
          Module: result.Module,
          Course: result.Course,
          ModCourseName: string,
          ModuleAbbrv: module.ModuleAbbrv,
          NoLect: L,
          NoPract: T,
          NoTut: P,
          TotalHr: totalHr
        }
        this.tempAssignmentArray.push(this.modCourseDisplay)
        console.log("modcourseran")

        // if (this.fullStaffAssignment[SNo] == null) {
        //   this.fullStaffAssignment[SNo] = [];
        // };


        const existing = this.fullStaffAssignment.map(x => x['staff']);
        if (existing.indexOf(SNo) != -1) {
          this.fullStaffAssignment[existing.indexOf(SNo)]['assignments'].push(this.modCourseDisplay);
          this.fullStaffAssignment[this.fullStaffAssignment.map(x => x['staff']).indexOf(SNo)]['TotalHr'] += totalHr;
        } else {
          this.fullStaffAssignment.push({ staff: SNo, staffName: SName, TotalHr: totalHr, assignments: [] });
          this.fullStaffAssignment[this.fullStaffAssignment.length - 1]['assignments'].push(this.modCourseDisplay);
        };
        // console.log("TEMP ASSIGNMENT ARRAY", this.tempAssignmentArray)
      }
    }
    // console.log("modcourseDisplay", this.modCourseDisplay);`
    console.log('testing', this.fullStaffAssignment);
    this.dataSource = new MatTableDataSource(this.fullStaffAssignment); // this.staffsectionList after filter section. 
    this.dataSource.paginator = this.paginator
  }



}

