import { Component, OnInit } from '@angular/core';
import { TasService, ForecastAssignment, AcadYearSem } from 'src/app/services/tas.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";


export interface modules {
  moduleID: string;
  moduleName: string;
}


@Component({
  selector: 'app-forecast-assign',
  templateUrl: './forecast-assign.component.html',
  styleUrls: ['./forecast-assign.component.css']
})

export class ForecastAssignComponent implements OnInit {

  moduleKey: object;

  FCLecture: number;

  FCTutorial: number;

  FCPractical: number;

  FCEdit: FormGroup;

  forecastList: ForecastAssignment

  AcadYearSem: AcadYearSem;

  newForecastList: ForecastAssignment

  constructor(private tasService: TasService) { }

  ngOnInit(): void {
    // get modulekey(s) from service
    this.tasService.currentYearSem.subscribe(info => {
      // @ts-ignore
      this.AcadYearSem = info
      console.log("acad", this.AcadYearSem)

      this.tasService.currentModKey.subscribe(modkey => {
        this.moduleKey = modkey;
        console.log('modkey', this.moduleKey);
        this.loadCurrentFC(this.moduleKey)
      })
    });

    this.FCEdit = new FormGroup({
      'FCStudent': new FormControl(null, [Validators.required]),
      'FCLecture': new FormControl(null, [Validators.required]),
      'FCTutorial': new FormControl(null, [Validators.required]),
      'FCPractical': new FormControl(null, [Validators.required])
    })



  }

  moduleList: modules[] = [
    { moduleID: '1', moduleName: 'module' }
  ]

  editForecast() {
    if (this.FCEdit.valid) {
      this.newForecastList = {
        TotalLectClass: this.FCEdit.value.FCLecture,
        TotalTutClass: this.FCEdit.value.FCTutorial,
        TotalPractClass: this.FCEdit.value.FCPractical,
        StuStageQty: this.FCEdit.value.FCStudent,
        Module: this.forecastList.Module,
        Course: this.forecastList.Course,
        Year: this.forecastList.Year,
        Sem: this.forecastList.Sem
      }
      this.tasService.updateForecastAssignment(this.newForecastList, this.AcadYearSem).subscribe()
    }

  }

  loadCurrentFC(object) {
    this.tasService.loadForecastAssignment(object.Course, object.ModuleID, this.AcadYearSem).subscribe((result) => {
      this.forecastList = result;
      console.log("forecastList", this.forecastList)

      this.FCEdit = new FormGroup({
        'FCStudent': new FormControl(this.forecastList.StuStageQty, [Validators.required]),
        'FCLecture': new FormControl(this.forecastList.TotalLectClass, [Validators.required]),
        'FCTutorial': new FormControl(this.forecastList.TotalTutClass, [Validators.required]),
        'FCPractical': new FormControl(this.forecastList.TotalPractClass, [Validators.required])
      })


    });
  }


  // editforecast/assignemnt/save TAS/ 
}
