import { Component, OnInit } from '@angular/core';
import { RoutingService } from '../../services/routing.service';
import { AuthService } from "../../auth/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private routing: RoutingService, public authService: AuthService, public router: Router) { }

  staffLevel : number;

  ngOnInit() {
    
  }

  onLogout() {
    this.authService.logout()
  }

  check() {
    this.staffLevel = this.authService.getLevel();
    console.log(this.staffLevel);
    if (this.staffLevel != 5) {
      alert("You have no access to this page");
      this.router.navigate(["/main"]);
    }
  }
}
