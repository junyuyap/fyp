import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from '@angular/material/table';
import { StaffinfoService } from "../../services/staffinfo.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../auth/auth.service";

export interface Levels {
  Staff: string;
  Level: number
}

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})



export class StaffComponent implements OnInit {

  displayedColumns: string[] = ['sname', 'sid', 'section', 'action'];

  tempStaffNo: string;

  tempStaffDisplay: Staff[] = [];

  tempDisplay: Staff = { StaffNo: "", StaffName: "", Section: "" }

  tempStaff: Info = {
    StaffNo: "", StaffName: "", StaffType: "", Honorific: "", Designation: "", StaffEmail: "",
    StaffContactNo: "", StaffHPNo: "", StaffAbbrv: "", Remarks: "", Section: 0
  };

  sectionInfo: Section[] = [];

  staffInfo: Info[] = [];

  patchStaff: FormGroup;

  data = new MatTableDataSource<Staff>();

  level: Levels[] = [];

  tempLevel: Levels;


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private staffInfoService: StaffinfoService, private auth: AuthService) { }

  ngOnInit() {

    this.patchStaff = new FormGroup({
      'Section': new FormControl(null, [Validators.required]),
      'StaffNo': new FormControl(null, [Validators.required])
    });

    this, this.staffInfoService.loadLevels().subscribe((result) => {
      this.level = result
    })

    this.staffInfoService.loadSection().subscribe((result) => {

      var display: Staff[] = [];

      this.sectionInfo = result
      console.log(this.sectionInfo);
      for (let staff of this.staffInfo) {
        for (let section of this.sectionInfo) {
          if (staff.Section === section.SectionID) {
            display.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
            this.tempStaffDisplay.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
          }
        }
      }
      this.data = new MatTableDataSource<Staff>(display);
      this.data.paginator = this.paginator
    })

    this.staffInfoService.loadStaff().subscribe((result) => {
      console.log(result);
      var display: Staff[] = [];
      this.staffInfo = result;
      for (let staff of this.staffInfo) {
        for (let section of this.sectionInfo) {
          if (staff.Section === section.SectionID) {
            display.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
            this.tempStaffDisplay.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
          }
        }
      }
      this.data = new MatTableDataSource<Staff>(display);
      this.data.paginator = this.paginator
    });

    this.staffInfoService.staffListUpdate.subscribe(() => {
      this.staffInfoService.loadStaff().subscribe((result) => {
        var display: Staff[] = [];
        console.log(result);
        this.staffInfo = result;
        for (let staff of this.staffInfo) {
          for (let section of this.sectionInfo) {
            if (staff.Section === section.SectionID) {
              display.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
              this.tempStaffDisplay.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
            }
          }
        }
        this.data = new MatTableDataSource<Staff>(display);
        this.data.paginator = this.paginator
      });
    });
  };

  deleteStaff(StaffNo) {
    this.tempStaffNo = StaffNo;
    console.log(StaffNo);
  }

  updateStaff(StaffNo) {
    for (let staff of this.staffInfo) {
      if (staff.StaffNo === StaffNo) {
        this.tempStaff = staff;
      }
    }
    for (let staff of this.tempStaffDisplay) {
      if (staff.StaffNo === StaffNo) {
        this.tempDisplay = staff;
      }
    }
    // for (let staff of this.level) {
    //   if(staff.Staff === StaffNo) {
    //     this.tempLevel = staff;
    //     console.log(staff);
    //   }
    // }
    this.patchStaff = new FormGroup({
      'Section': new FormControl(this.tempDisplay.Section, [Validators.required]),
      'StaffNo': new FormControl(this.tempDisplay.StaffNo, [Validators.required])
      // 'StaffLevel': new FormControl(this.tempLevel.Level,  [Validators.required])
    });
  }

  confirmUpdate() {

    if (this.patchStaff.valid) {
      for (let section of this.sectionInfo) {
        if (this.patchStaff.value.Section === section.SectionName) {
          this.tempStaff.Section = section.SectionID;
        }
      }
      const temp = this.tempStaff.StaffNo;
      this.tempStaff.StaffNo = this.patchStaff.value.StaffNo;
      console.log(this.patchStaff.value.StaffNo);
      // this.tempLevel = this.patchStaff.value.StaffLevel;
      console.log(this.patchStaff.value.Section)
      this.staffInfoService.updateStaff( this.tempStaff, temp).subscribe();
      this.patchStaff.reset();
      alert("Staff section successfully updated!")
    }
    else {
      alert("All fields are required")
    }
    this.staffInfoService.staffListUpdate.subscribe(() => {
      this.staffInfoService.loadStaff().subscribe((result) => {
        var display: Staff[] = [];
        console.log(result);
        this.staffInfo = result;
        for (let staff of this.staffInfo) {
          for (let section of this.sectionInfo) {
            if (staff.Section === section.SectionID) {
              display.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
              this.tempStaffDisplay.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
            }
          }
        }
        this.data = new MatTableDataSource<Staff>(display);
        this.data.paginator = this.paginator
      });
    });
  }

  confirmDelete() {
    this.staffInfoService.removeStaff(this.tempStaffNo);
    this.staffInfoService.staffListUpdate.subscribe(() => {
      this.staffInfoService.loadStaff().subscribe((result) => {
        var display: Staff[] = [];
        console.log(result);
        this.staffInfo = result;
        for (let staff of this.staffInfo) {
          for (let section of this.sectionInfo) {
            if (staff.Section === section.SectionID) {
              display.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
              this.tempStaffDisplay.push({ StaffNo: staff.StaffNo, StaffName: staff.StaffName, Section: section.SectionName });
            }
          }
        }
        this.data = new MatTableDataSource<Staff>(display);
        this.data.paginator = this.paginator
      });
    });
  }


}

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

export interface Staff {
  StaffNo: string;
  StaffName: string;
  Section: string;
}

