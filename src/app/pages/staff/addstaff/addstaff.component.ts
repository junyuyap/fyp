import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { StaffinfoService } from "../../../services/staffinfo.service";
import { AuthService } from "../../../auth/auth.service";

export interface Info {
  StaffNo: string;
  StaffName: string;
  StaffType: string;
  Honorific: string;
  Designation: string;
  StaffEmail: string;
  StaffContactNo: string;
  StaffHPNo: string;
  StaffAbbrv: string;
  Remarks: string;
  Section: number;
}

export interface Section {
  Manager: string;
  SectionID: number;
  SectionName: string;
}

export interface Auth {
  staff: string;
  password: string;
  level: number;
}


@Component({
  selector: 'app-addstaff',
  templateUrl: './addstaff.component.html',
  styleUrls: ['./addstaff.component.css']
})
export class AddstaffComponent implements OnInit {

  constructor(private staffinfoservice: StaffinfoService, private authService: AuthService) { }

  addStaff: FormGroup;

  info: Info;

  auth: Auth;

  sectionList: Section[] = [];

  ngOnInit() {
    this.addStaff = new FormGroup({
      'StaffName': new FormControl(null, [Validators.required]),
      'StaffNo': new FormControl(null, [Validators.required]),
      'StaffType': new FormControl(null, [Validators.required]),
      'Honorific': new FormControl(null, [Validators.required]),
      'Designation': new FormControl(null, [Validators.required]),
      'StaffEmail': new FormControl(null, [Validators.required]),
      'StaffContactNo': new FormControl(null, [Validators.required]),
      'StaffHPNo': new FormControl(null, [Validators.required]),
      'StaffAbbrv': new FormControl(null, [Validators.required]),
      'StaffPass': new FormControl(null, [Validators.required]),
      'StaffLevel': new FormControl(null, [Validators.required]),
      'Section': new FormControl(null, [Validators.required])
    })
    this.staffinfoservice.loadSection().subscribe((result) => {
      this.sectionList = result;
      console.log(this.sectionList);
    })
  }

  onSubmit() {

    var temp: number = 0

    for (let section of this.sectionList) {
      if (this.addStaff.value.Section === section.SectionName) {
        temp = section.SectionID;
      }
    }

    if (this.addStaff.valid) {
      this.info = {
        "StaffNo": this.addStaff.value.StaffNo,
        "StaffName": this.addStaff.value.StaffName,
        "StaffType": this.addStaff.value.StaffType,
        "Honorific": this.addStaff.value.Honorific,
        "Designation": this.addStaff.value.Designation,
        "StaffEmail": this.addStaff.value.StaffEmail,
        "StaffContactNo": this.addStaff.value.StaffContactNo,
        "StaffHPNo": this.addStaff.value.StaffHPNo,
        "StaffAbbrv": this.addStaff.value.StaffAbbrv,
        "Remarks": null,
        "Section": temp
      }
      this.auth = {
        "staff": this.addStaff.value.StaffNo,
        "password": this.addStaff.value.StaffPass,
        "level": this.addStaff.value.StaffLevel
      }
      this.staffinfoservice.addStaff(this.info);
      this.staffinfoservice.staffListUpdate.subscribe(() => {
        this.staffinfoservice.register(this.auth);
      });

      alert("Staff successfully added!")
    }
    else {
      alert("One or more of the fields are invalid!")
    }
  }

}

