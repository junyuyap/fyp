import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NgControlStatus } from '@angular/forms';

interface Name {
  value: string;
  viewValue: string;
}

interface Role {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})


export class AdminComponent implements OnInit {


  @ViewChild('form1') form1: ElementRef;

  @ViewChild('form2') form2: ElementRef;

  constructor() { }

  ngOnInit() {

  }

  test(acadYear) {
    this.form2.nativeElement.action = `https://tas-backend.herokuapp.com/api/uploads/projection?acadYear=${acadYear}`;
    console.log(this.form2.nativeElement.action);
    this.form2.nativeElement.submit();
  }

}
