import { Component, OnInit } from '@angular/core';
import { TasService, Course } from 'src/app/services/tas.service';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from "@angular/forms";


@Component({
  selector: 'app-courseinfo',
  templateUrl: './courseinfo.component.html',
  styleUrls: ['./courseinfo.component.css']
})
export class CourseinfoComponent implements OnInit {

  constructor(private tasService: TasService) { }
  
  courseList: Course[] = [];

  deleteInfo;

  tempCourseInfo: Course;

  newCourse: FormGroup;

  displayedColumns: string[] = ['cid', 'cname', 'action'];
  data = new MatTableDataSource<Course>();

  ngOnInit(): void {
    this.tasService.loadCourse().subscribe((result) => {
      this.courseList = result;
      console.log(this.courseList)
      this.data = new MatTableDataSource<Course>(this.courseList)
    });

    this.newCourse = new FormGroup({
      'NCAbbrv': new FormControl(null, [Validators.required]),
      'NCName': new FormControl(null, [Validators.required])
    })
  }

  deleteCourse(info) {
    this.deleteInfo = info
    console.log(this.deleteInfo)
  }
  

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.data.filter = filterValue;
  }

  confirmDelete() {
    this.tasService.removeCourse(this.deleteInfo)
    this.resolveAfter1Seconds(20).then(value => {
      this.tasService.loadCourse().subscribe((result) => {
        this.courseList = result;
        console.log(this.courseList)
        this.data = new MatTableDataSource<Course>(this.courseList)
      });
    });
  }

  resolveAfter1Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 1000);
    });
  }

  addCourse() {
    if(this.newCourse.valid) {  
      this.tempCourseInfo = {   
      CourseAbbrv: this.newCourse.value.NCAbbrv.toUpperCase(),
      CourseName: this.newCourse.value.NCName}
      console.log(this.tempCourseInfo)
      this.tasService.addCourse(this.tempCourseInfo);
      // Reload Course Table
      this.resolveAfter1Seconds(20).then(value => {
        this.tasService.loadCourse().subscribe((result) => {
          this.courseList = result;
          console.log(this.courseList)
          this.data = new MatTableDataSource<Course>(this.courseList)
        });
      });
    }
  }

}
