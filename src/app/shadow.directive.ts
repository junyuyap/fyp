import { Directive, ElementRef, Renderer2, HostBinding, HostListener} from '@angular/core';


@Directive({
  selector: '[appShadow]'
})
export class ShadowDirective {

  constructor() { }

  @HostBinding('style.box-shadow') shadow: string

  @HostListener ('mouseenter') mouseOver(eventData: Event) {
    this.shadow = '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
  }

  @HostListener ('mouseleave') mouseOut(eventData: Event) {
    this.shadow = 'none'
  }

}
